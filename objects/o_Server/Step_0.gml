/// @description increment idle time
if(!o_Simulator.paused){
if(!busy){
	if(hasToQuit){
		with(activeModel){
			serverAmount--;
			balkingCap = 4*serverAmount;
			ds_list_delete(servers, ds_list_size(servers)-1);
			aServerIsQuitting = false;
		}
		instance_destroy();
	}
	else if(hasToQuitA){
		with(activeModel){
			serverAmountA--;
			balkingCap = 3*(serverAmountA+serverAmountB+serverAmountC);
			ds_list_delete(serversA, ds_list_size(serversA)-1);
			aServerIsQuitting = false;
		}
		instance_destroy();
	}
	else if(hasToQuitB){
		with(activeModel){
			serverAmountB--;
			balkingCap = 3*(serverAmountA+serverAmountB+serverAmountC);
			ds_list_delete(serversB, ds_list_size(serversB)-1);
			aServerIsQuitting = false;
		}
		instance_destroy();
	}
	else if(hasToQuitC){
		with(activeModel){
			serverAmountC--;
			balkingCap = 3*(serverAmountA+serverAmountB+serverAmountC);
			ds_list_delete(serversC, ds_list_size(serversC)-1);
			aServerIsQuitting = false;
		}
		instance_destroy();
	}
	if(activeModel.renegWait <= 0){
		idleTime++;
		with(activeModel)
			idleTimeCounter++;
	}
}
aliveTime++
with(activeModel)
	aliveTimeCounter++;
}