/// @description --
/// @description new clients arrive
if(!o_Simulator.paused){
//checking if server idle is found when people are waiting
if(!ds_list_empty(waitingQueue)){
	idleServerFound = false;
	for(i = 0; i<ds_list_size(servers) && !idleServerFound; i++){
		var serv = ds_list_find_value(servers, i);
		if(!serv.busy){
			if(specialRenegsPointer < ds_list_size(specialRenegs) && ds_list_find_value(specialRenegs, specialRenegsPointer) == 0){
				renegWait = 60 * o_Simulator.timeRatio * 2;
				specialRenegsPointer++;
			}
			if(renegWait <= 0){
				var newServedClient = ds_list_find_value(waitingQueue, 0);
				newServedClient.beingServed = true;
				newServedClient.serverID = serv.id;
				newServedClient.destinationX = serv.x
				newServedClient.destinationY = serv.y + 50
				newServedClient.wandering = false;
				serv.busy = true;
				idleServerFound = true;	
				ds_list_delete(waitingQueue, 0);
				for(var i = specialRenegsPointer; i<ds_list_size(specialRenegs); i++){
					ds_list_replace(specialRenegs, i, ds_list_find_value(specialRenegs, i) - 1);
				}
			}
			renegWait--;
		}			
	}
}
//cheking if machine idle is found when people are waiting
if(!ds_list_empty(waitingNumber)){
	idleMachineFound = false;
	for(i = 0; i<ds_list_size(machines) && !idleMachineFound; i++){
		var mach = ds_list_find_value(machines, i);
		if(!mach.busy){
			var newServedClient = ds_list_find_value(waitingNumber, 0);
			newServedClient.beingServed = true;
			newServedClient.serverID = mach.id;
			newServedClient.destinationX = mach.x
			newServedClient.destinationY = mach.y + 50
			mach.busy = true;
			idleMachineFound = true;	
			ds_list_delete(waitingNumber, 0);			
		}			
	}
}

// Arrival && balking logic
if(arrivalCounter>=nextArrival){
	// There is a new client, where to go?
	arrivalCounter = 0;
	arrivedClients++;
	with(o_Simulator)
		other.nextArrival = ds_list_find_value(arrivalsList, other.arrivedClients);
	
	if(ds_list_size(waitingNumber) + ds_list_size(waitingQueue) > balkingCap){	//balking ?
		if(random(100)>=50){ //balking prob
			balkingAmount++;
			balk = true;
		}
	}
	if(!balk){
		if(ds_list_empty(waitingNumber)){
			idleMachineFound = false;
			for(i = 0; i<ds_list_size(machines) && !idleMachineFound; i++){
				var mach = ds_list_find_value(machines, i);
				if(!mach.busy){
					var newClient = instance_create_layer(entryX, entryY, "Clients", o_Client);
					newClient.destinationX = mach.x;
					newClient.destinationY = mach.y + 50;
					newClient.beingServed = true;
					newClient.serverID = mach.id;
					newClient.activeModel = object_index;
					newClient.waitAtNumberMachine = true;
					mach.busy = true;
					idleMachineFound = true;	
				}			
			}
			if(!idleMachineFound){
				//first waiting client
				var newClient = instance_create_layer(entryX, entryY, "Clients", o_Client);
				newClient.activeModel = object_index;
				newClient.waitAtNumberMachine = true;
				ds_list_insert(waitingNumber, 0, newClient)
			}
		}
		else{
			//locate new client at entry
			var newClient = instance_create_layer(entryX, entryY, "Clients", o_Client);
			newClient.activeModel = object_index;
			newClient.waitAtNumberMachine = true;
			ds_list_insert(waitingNumber, ds_list_size(waitingNumber), newClient)
		}
	}
	balk = false;
}
arrivalCounter++;

//update destination of all waiting clients
if(!ds_list_empty(waitingQueue)){
	for(i = 0; i<ds_list_size(waitingQueue); i++){
		var toMove = ds_list_find_value(waitingQueue, i);
		var rand = random_range(0, 99)
		if(toMove.destinationY - 1 <= toMove.y && toMove.y <= toMove.destinationY + 1 && toMove.destinationX - 1 <= toMove.x && toMove.x <= toMove.destinationX + 1 && rand > 90){		
			var placeX = random_range(o_WaitingZone.x + toMove.sprite_width, o_WaitingZone.x + o_WaitingZone.sprite_width - toMove.sprite_width);
			var placeY = random_range(o_WaitingZone.y + toMove.sprite_height, o_WaitingZone.y + o_WaitingZone.sprite_height - toMove.sprite_height);
			toMove.destinationX = placeX;	
			toMove.destinationY = placeY;
		}
	}
}
if(!ds_list_empty(waitingNumber)){
	var moveFirst = ds_list_find_value(waitingNumber, 0);
	moveFirst.destinationX = ds_list_find_value(machines, 0).x;
	moveFirst.destinationY = ds_list_find_value(machines, 0).y + 160;
	for(i = 1; i<ds_list_size(waitingNumber); i++){
		var toMove = ds_list_find_value(waitingNumber, i);
		var reference = ds_list_find_value(waitingNumber, i-1);
		toMove.destinationX = reference.destinationX + 80;	
	}
}

var allowanceToChange = ((o_Simulator.current_timer/o_Simulator.end_timer*o_Simulator.simTime) mod (serversDelay/60)) == 0;
if(o_Simulator.current_timer == 0)
	allowanceToChange = 0;

if(allowanceToChange){
	//new server
	if(ds_list_size(waitingQueue)> waitThreshold && !recentlyAdded){
		if(!aServerIsQuitting && serverAmount<maxServers){
			serverAmount++;
			balkingCap = 4*serverAmount;
			var newServ = instance_create_layer(x+80*(ds_list_size(servers)+1), y+100, "Servers", o_Server);
			newServ.activeModel = object_index;
			ds_list_insert(servers, ds_list_size(servers), newServ);
			recentlyAdded = true;
			alarm[0] = 60* o_Simulator.timeRatio * (serversDelay-1);
		}
	}

	//remove server
	if(serverAmount>1 && !recentlyAdded){
		var removeServer = false;
		var sum = 0;
		for(i = 0; i<ds_list_size(servers); i++){
			var serv = ds_list_find_value(servers, i);
			var servRatio = round((serv.aliveTime-serv.idleTime)/serv.aliveTime*100);
			sum += servRatio;
			if(servRatio > 0 && servRatio < useThreshold)
				removeServer = true;		
		}
		if(sum/ds_list_size(servers) <= (100 - 100/ds_list_size(servers)))
			removeServer = true;
		if(removeServer){
			aServerIsQuitting = true;
			var toDestroy = ds_list_find_value(servers, ds_list_size(servers)-1);
			//ToDestroy
			toDestroy.hasToQuit = true;
		}
	}
}
}