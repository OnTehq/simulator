{
    "id": "3519f27c-c72b-414c-aeef-bc90daf80a6b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_button",
    "eventList": [
        {
            "id": "cc05a67f-f9b4-44af-914e-822603b534c5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "3519f27c-c72b-414c-aeef-bc90daf80a6b"
        },
        {
            "id": "a5452c5f-bca1-4c8e-bfcb-9d63e4e0686b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3519f27c-c72b-414c-aeef-bc90daf80a6b"
        },
        {
            "id": "34760998-6a0f-485a-ab1b-877c70e8283c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "3519f27c-c72b-414c-aeef-bc90daf80a6b"
        },
        {
            "id": "5592af30-9e2f-4a3d-8455-ad1095acc213",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "3519f27c-c72b-414c-aeef-bc90daf80a6b"
        },
        {
            "id": "715483ae-9f2e-43b4-afa4-c76b72fdfd7e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "3519f27c-c72b-414c-aeef-bc90daf80a6b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "31c9739c-cb7f-4de3-8914-cb7415dd67ec",
    "visible": true
}