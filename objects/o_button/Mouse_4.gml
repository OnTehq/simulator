/// @description --
with(o_UserInput){
	switch(typeOfInput){
		case 0 :
			o_VariablesBank.arrs = real(text);
			break;
		case 1 :
			o_VariablesBank.serv = real(text);
			break;
		case 2 :
			o_VariablesBank.stime = real(text);
			break;
		case 3 :
			o_VariablesBank.sdur = real(text);
			break;
		case 4:
			o_VariablesBank.sipps = real(text);
			break;	
		case 5:
			o_VariablesBank.srvamnt = real(text);
			break;	
		case 6:
			o_VariablesBank.cratio = real(text);
			break;	
		case 7:
			o_VariablesBank.wtrshold = real(text);
			break;	
		case 8:
			o_VariablesBank.wdelay = real(text);
			break;	
		case 9:
			o_VariablesBank.usfltrshold = real(text);
			break;	
		case 10:
			o_VariablesBank.mxsrv = real(text);
			break;	
	}
}
draw_set_color(c_white);
room_goto_next();