/// @description General values for all three simulators
randomize();

//collect several user choices
with(o_VariablesBank){
		other.amountOfServ = srvamnt;
		other.cheatingRatio = cratio;
		other.waitingThreshold = wtrshold;
		other.delayToChange = wdelay;
		other.usefulThreshold = usfltrshold;
		other.maxServerAmount = mxsrv;
}

//in data, a value represents a minute
simTime = o_DataAnalysis.simulatedTime; //how much time is simulated in hours
simDuration = o_DataAnalysis.simulationDuration; //how much the sim has to last in minutes
timeRatio = simDuration/simTime;
end_timer = simTime * 3600 * timeRatio; //ending of simulation in ticks

clientArrivalRNs = o_DataAnalysis.arrivalProbs;
for(i = 0; i< ds_list_size(clientArrivalRNs); i++){
	ds_list_replace(clientArrivalRNs, i, floor(ds_list_find_value(clientArrivalRNs, i) * 100 - 1));
}
clientServiceRNs = o_DataAnalysis.serviceProbs;
for(i = 0; i< ds_list_size(clientServiceRNs); i++){
	ds_list_replace(clientServiceRNs, i, floor(ds_list_find_value(clientServiceRNs, i) * 100 - 1));
}
averageArrivals = o_DataAnalysis.averagePerCatArrival;
averageServices = o_DataAnalysis.averagePerCatService;
minServiceTime = o_DataAnalysis.minService;
maxServiceTime = o_DataAnalysis.maxService;
sippAmount = o_DataAnalysis.totalSippAmount;

clientArrivalCategories = o_DataAnalysis.arrivalCatAmount
clientServiceCategories = o_DataAnalysis.serviceCatAmount

currentSippPointer = 0;
//simulatedTime = real(simulatedTime);

//Generate all arrivals
var arrivalSum = 0;
arrivalsList = ds_list_create();
while(arrivalSum < end_timer){
	var RN = random_range(0,99);
	var currentSipp = floor(arrivalSum/end_timer*(sippAmount+1));
	var found = false;
	var avgIndex = -1;
	for(i = currentSipp*clientArrivalCategories; i < (currentSipp*clientArrivalCategories + clientArrivalCategories) && !found; i++){
		if(RN <= ds_list_find_value(clientArrivalRNs, i)){
			found = true;
			avgIndex = i - currentSipp*clientArrivalCategories;
			var newArrivalTime = 60 * timeRatio * ds_list_find_value(averageArrivals, avgIndex);
			ds_list_add(arrivalsList, newArrivalTime)
			arrivalSum += newArrivalTime;		
		}
	}
}

//Generate all services
servicesList = ds_list_create();
serviceCounter = 0;
for(i = 0; i<ds_list_size(arrivalsList);i++){
	var RN = random_range(0,99);
	var found = false;
	for(j = 0; j < clientServiceCategories && !found; j++){
		if(RN <= ds_list_find_value(clientServiceRNs, j)){
			found = true;
			ds_list_add(servicesList, 60 * timeRatio * ds_list_find_value(averageServices, j));
		}
	}
}

//OUTPUT COLLECTION

MMSSippServed=ds_list_create();
MMSpreviousServedClient = 0;
MMSSippBalked=ds_list_create();
MMSpreviousBalked = 0;
MMSSippReneged=ds_list_create();
MMSpreviousReneged = 0;
MMSSippServiceTime=ds_list_create();
MMSpreviousServiceTime = 0;
MMSSippServiceRatio=ds_list_create();
MMSpreviousIdleTime = 0;


PaNSippServed=ds_list_create();
PaNpreviousServedClient = 0;
PaNSippBalked=ds_list_create();
PaNpreviousBalked = 0;
PaNSippReneged=ds_list_create();
PaNpreviousReneged = 0;
PaNSippServiceTime=ds_list_create();
PaNpreviousServiceTime = 0;
PaNSippServiceRatio=ds_list_create();
PaNpreviousIdleTime = 0;


CTRTSippServed=ds_list_create();
CTRTpreviousServedClient = 0;
CTRTSippBalked=ds_list_create();
CTRTpreviousBalked = 0;
CTRTSippReneged=ds_list_create();
CTRTpreviousReneged = 0;
CTRTSippServiceTime=ds_list_create();
CTRTpreviousServiceTime = 0;
CTRTSippServiceRatio=ds_list_create();
CTRTpreviousIdleTime = 0;


room_goto_next();