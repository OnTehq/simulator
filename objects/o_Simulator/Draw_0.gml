/// @description --
if(!ended){
	draw_rectangle(1050, 700, 1150, 800, true);
	var toPrint = 8 + current_timer/end_timer*simTime;
	var hours = floor(toPrint);
	var minutes = floor((toPrint - floor(toPrint)) * 60);
	draw_text(1055, 730, string(hours)+"h"+string(minutes));
}
else{
	draw_set_halign(fa_center);
	draw_set_valign(fa_center);
	draw_text(room_width/2, room_height/10, "Final Results {Served, Balking, Reneging, ServiceTime, ServiceRatio}");
	draw_text(room_width/4, room_height*2/10, "M/M/S");
	draw_text(room_width*3/5, room_height*2/10, "Pick a Number");
	draw_text(room_width*17/20, room_height*2/10, "Roma Termini/El Corte Ingles");
	draw_set_halign(fa_left);
	for(var j= 1; j<=sippAmount+1;j++){
		draw_text(room_width/10, room_height*2/10 + room_height*j/10, "SIPP" + string(j));
	}
	draw_text(room_width/10, room_height*9/10, "Total");
	for(var i = 1; i<=3; i++){
		for(var j= 1; j<=sippAmount+1;j++){
			switch(i){
				case 1:
					draw_text(room_width*2/10+room_width*3/10*(i-1), room_height*2/10 + room_height*j/10,"{ " + string(ds_list_find_value(MMSSippServed, j))+ "; "+ string(ds_list_find_value(MMSSippBalked, j)) + "; "+ string(ds_list_find_value(MMSSippReneged, j))+ "; "+ string(ds_list_find_value(MMSSippServiceTime, j))+ "; "+ string(ds_list_find_value(MMSSippServiceRatio, j))+ " }");
					draw_text(room_width*2/10+room_width*3/10*(i-1), room_height*9/10, "{ " + string(FinalMMSServedClients) + "; "+string(FinalMMSBalkingAmount)+ "; "+ string(FinalMMSRenegingAmount)+ "; "+ string(FinalMMSServiceTime)+ "; "+ string(FinalMMSServiceRatio)+ " }");
					break;
				case 2:
					draw_text(room_width*2/10+room_width*3/10*(i-1), room_height*2/10 + room_height*j/10, "{ " + string(ds_list_find_value(PaNSippServed, j))+ "; "+ string(ds_list_find_value(PaNSippBalked, j)) + "; "+ string(ds_list_find_value(PaNSippReneged, j))+ "; "+ string(ds_list_find_value(PaNSippServiceTime, j))+ "; "+ string(ds_list_find_value(PaNSippServiceRatio, j))+ " }");
					draw_text(room_width*2/10+room_width*3/10*(i-1), room_height*9/10, "{ " + string(FinalPaNServedClients)+ "; "+string(FinaPaNBalkingAmount)+ "; "+ string(FinalPaNRenegingAmount)+ "; "+ string(FinalPaNServiceTime)+ "; "+ string(FinalPaNServiceRatio)+ " }");
					break;
				case 3:
					draw_text(room_width*2/10+room_width*3/10*(i-1), room_height*2/10 + room_height*j/10, "{ " + string(ds_list_find_value(CTRTSippServed, j))+ "; "+ string(ds_list_find_value(CTRTSippBalked, j)) + "; "+ string(ds_list_find_value(CTRTSippReneged, j))+ "; "+ string(ds_list_find_value(CTRTSippServiceTime, j))+ "; "+ string(ds_list_find_value(CTRTSippServiceRatio, j))+ " }");
					draw_text(room_width*2/10+room_width*3/10*(i-1), room_height*9/10, "{ " + string(FinalCTRTServedClients)+ "; "+string(FinalCTRTBalkingAmount)+ "; "+ string(FinalCTRTRenegingAmount)+ "; "+ string(FinalCTRTServiceTime)+ "; "+ string(FinalCTRTServiceRatio)+ " }");
					break;
			}
		}	
	}

	
}
