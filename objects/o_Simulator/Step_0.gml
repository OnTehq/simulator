/// @description setting of parameters from Monte-Carlo based on data


//OUTPUT LOGIC 
if((current_timer/end_timer)>= 1/(sippAmount+1)*currentSippPointer){
	
	//MMS
	{
	ds_list_add(MMSSippServed, o_MMS.servedClients - MMSpreviousServedClient);
	MMSpreviousServedClient = o_MMS.servedClients;
	ds_list_add(MMSSippBalked, o_MMS.balkingAmount - MMSpreviousBalked);
	MMSpreviousBalked = o_MMS.balkingAmount;
	ds_list_add(MMSSippReneged, o_MMS.renegingAmount - MMSpreviousReneged);
	MMSpreviousReneged = o_MMS.renegingAmount;
	var servtime = (o_MMS.aliveTimeCounter - MMSpreviousServiceTime)/60/timeRatio
	ds_list_add(MMSSippServiceTime, servtime - servtime mod min(delayToChange, simTime*60/(sippAmount+1)));
	MMSpreviousServiceTime = o_MMS.aliveTimeCounter;
	var idletime = (o_MMS.idleTimeCounter - MMSpreviousIdleTime)/60/timeRatio
	idletime = idletime - idletime mod min(delayToChange, simTime*60/(sippAmount+1))
	ds_list_add(MMSSippServiceRatio, (servtime-idletime)/servtime);
	MMSpreviousIdleTime = o_MMS.idleTimeCounter;
	}

	
	//PaN
	{
	ds_list_add(PaNSippServed, o_PaN.servedClients - PaNpreviousServedClient);
	PaNpreviousServedClient = o_PaN.servedClients;
	ds_list_add(PaNSippBalked, o_PaN.balkingAmount - PaNpreviousBalked);
	PaNpreviousBalked = o_PaN.balkingAmount;
	ds_list_add(PaNSippReneged, o_PaN.renegingAmount - PaNpreviousReneged);
	PaNpreviousReneged = o_PaN.renegingAmount;
	var servtime = (o_PaN.aliveTimeCounter - PaNpreviousServiceTime)/60/timeRatio
	ds_list_add(PaNSippServiceTime, servtime - servtime mod min(delayToChange, simTime*60/(sippAmount+1)));
	PaNpreviousServiceTime = o_PaN.aliveTimeCounter;
	var idletime = (o_PaN.idleTimeCounter - PaNpreviousIdleTime)/60/timeRatio
	idletime = idletime - idletime mod min(delayToChange, simTime*60/(sippAmount+1))
	ds_list_add(PaNSippServiceRatio, (servtime-idletime)/servtime);
	PaNpreviousIdleTime = o_PaN.idleTimeCounter;
	}
	
	//CT_RT
	{
	ds_list_add(CTRTSippServed, o_CT_RT.servedClients - CTRTpreviousServedClient);
	CTRTpreviousServedClient = o_CT_RT.servedClients;
	ds_list_add(CTRTSippBalked, o_CT_RT.balkingAmount - CTRTpreviousBalked);
	CTRTpreviousBalked = o_CT_RT.balkingAmount;
	ds_list_add(CTRTSippReneged, o_CT_RT.renegingAmount - CTRTpreviousReneged);
	CTRTpreviousReneged = o_CT_RT.renegingAmount;
	var servtime = (o_CT_RT.aliveTimeCounter - CTRTpreviousServiceTime)/60/timeRatio
	ds_list_add(CTRTSippServiceTime, servtime - servtime mod min(delayToChange, simTime*60/(sippAmount+1)));
	CTRTpreviousServiceTime = o_CT_RT.aliveTimeCounter;
	var idletime = (o_CT_RT.idleTimeCounter - CTRTpreviousIdleTime)/60/timeRatio
	idletime = idletime - idletime mod min(delayToChange, simTime*60/(sippAmount+1))
	ds_list_add(CTRTSippServiceRatio, (servtime-idletime)/servtime);
	CTRTpreviousIdleTime = o_CT_RT.idleTimeCounter;
	}
	
	currentSippPointer++;
}


if(current_timer >= end_timer && !ended){
	ended = true;
	FinalMMSBalkingAmount = o_MMS.balkingAmount;
	FinalMMSRenegingAmount = o_MMS.renegingAmount;
	FinalMMSServedClients = o_MMS.servedClients;
	FinalMMSServiceTime = o_MMS.aliveTimeCounter/60/timeRatio
	FinalMMSServiceTime =  FinalMMSServiceTime - FinalMMSServiceTime mod min(delayToChange, simTime*60);
	FinalMMSServiceRatio = o_MMS.idleTimeCounter/60/timeRatio
	FinalMMSServiceRatio =  FinalMMSServiceRatio - FinalMMSServiceRatio mod min(delayToChange, simTime*60);
	FinalMMSServiceRatio = (FinalMMSServiceTime-FinalMMSServiceRatio)/FinalMMSServiceTime
	
	FinaPaNBalkingAmount = o_PaN.balkingAmount;
	FinalPaNRenegingAmount = o_PaN.renegingAmount;
	FinalPaNServedClients = o_PaN.servedClients;
	FinalPaNServiceTime = o_PaN.aliveTimeCounter/60/timeRatio
	FinalPaNServiceTime =  FinalPaNServiceTime - FinalPaNServiceTime mod min(delayToChange, simTime*60);
	FinalPaNServiceRatio = o_PaN.idleTimeCounter/60/timeRatio
	FinalPaNServiceRatio =  FinalPaNServiceRatio - FinalPaNServiceRatio mod min(delayToChange, simTime*60);
	FinalPaNServiceRatio = (FinalPaNServiceTime-FinalPaNServiceRatio)/FinalPaNServiceTime
	
	FinalCTRTBalkingAmount = o_CT_RT.balkingAmount;
	FinalCTRTRenegingAmount = o_CT_RT.renegingAmount;
	FinalCTRTServedClients = o_CT_RT.servedClients;
	FinalCTRTServiceTime = o_CT_RT.aliveTimeCounter/60/timeRatio
	FinalCTRTServiceTime =  FinalCTRTServiceTime - FinalCTRTServiceTime mod min(delayToChange, simTime*60);
	FinalCTRTServiceRatio = o_CT_RT.idleTimeCounter/60/timeRatio
	FinalCTRTServiceRatio =  FinalCTRTServiceRatio - FinalCTRTServiceRatio mod min(delayToChange, simTime*60);
	FinalCTRTServiceRatio = (FinalCTRTServiceTime-FinalCTRTServiceRatio)/FinalCTRTServiceTime
	
	room_goto(r_end);
}

if(!ended && !paused)
	current_timer++;
