/// @description --
draw_set_font(fnt_simulatorFont)

draw_set_halign(fa_center);
draw_set_valign(fa_center);
draw_text(room_width/2, room_height/4, "Graphical informations");


draw_text(room_width/5, room_height/2, "This is a client \nThey can be:");
draw_text(room_width*2/5, room_height/2, "This is a server \nThey can be:");
draw_text(room_width*3/5, room_height/2, "This is a machine \nIt can be:");
draw_text(room_width*4/5, room_height/2, "This is the ticket\nwaiting zone:");

draw_text(x+280, y+650, "Waiting");
draw_text(x+410, y+650, "Served");

draw_text(x+658, y+650, "Idle");
draw_text(x+780, y+650, "Busy");

draw_text(x+1020, y+650, "Idle");
draw_text(x+1140, y+650, "Busy");
