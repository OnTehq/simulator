/// @description --
draw_set_font(fnt_Menu);

draw_set_halign(fa_left);
draw_set_valign(fa_left)
draw_text(50, room_height/4, "Queue simulator");

draw_set_font(fnt_Menu_small);
draw_text(50, room_height/2, "Simulating M/M/S, Pick a Number and \nRoma Termini/El Corte Ingles Models");
draw_text(50, room_height*3/4, "Press space to begin");


draw_set_font(fnt_simulatorFont);