/// @description Color if served
if(beingServed)
	sprite_index = s_Client_served;
if(!beingServed)
	sprite_index = s_Client_waiting;
if(reneg)
	sprite_index = s_Client_reneging;
if(leaving)
	sprite_index = s_Client_leaving;
	
	
image_angle = point_direction(x,y, destinationX, destinationY)	+ 180

draw_self();


draw_set_font(fnt_simulatorFontSmall);
draw_text(x+10, y+10, string(floor(serviceTimeWanted/60/o_Simulator.timeRatio))+"min");
draw_set_font(fnt_simulatorFont);



