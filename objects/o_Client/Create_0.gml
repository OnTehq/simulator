/// @description --
//image_speed = 0;

var serviceLowerLimit = o_Simulator.minServiceTime;
var serviceUpperLimit = o_Simulator.maxServiceTime;
var serviceWidth = (serviceUpperLimit - serviceLowerLimit)/3;

SERVICE_TIME_SHORT_LIMIT = 60 * o_Simulator.timeRatio * (serviceLowerLimit + serviceWidth);
SERVICE_TIME_MEDIUM_LIMIT = 60 * o_Simulator.timeRatio * (serviceLowerLimit + 2 * serviceWidth);

NUMBER_MACHINE_TIME = o_Simulator.timeRatio * 60
SERVICE_MACHINE_TIME = o_Simulator.timeRatio * 60

//serviceTimeWanted = choose(SERVICE_TIME_SHORT, SERVICE_TIME_MEDIUM, SERVICE_TIME_LONG);

//select time based on data
serviceTimeWanted = ds_list_find_value(o_Simulator.servicesList, floor(o_Simulator.serviceCounter/3));
o_Simulator.serviceCounter++;

//adjust reneging cap
renegingCap = 3*serviceTimeWanted;

cheatRatio = o_Simulator.cheatingRatio;