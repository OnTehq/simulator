/// @description serving logic
if(!o_Simulator.paused){

realSpeed = speedOfWalk * (24/o_Simulator.timeRatio);

if(wandering)
	realSpeed = realSpeed * 0.75

var dx = destinationX - x
var dy = destinationY - y
var dist = sqrt(power(dx, 2)+power(dy,2));	
var t = dist / realSpeed;
var vx = dx/t;
var vy = dy/t;

if(!(x < destinationX + 1 && x > destinationX - 1)){
	if(abs(vx) > abs(dx))
		vx = dx;
	x += vx; 
}
if(!(y < destinationY + 1 && y > destinationY - 1)){
	if(abs(vy) > abs(dy))
		vy = dy;
	y += vy; 	
}


//end of punishment
if(punished && y > destinationY - 20 && y < destinationY + 20 && x > destinationX - 20 && x < destinationX + 20){
	with(o_CT_RT){	
		if(ds_list_find_index(waitingQueueA, other) != -1)
			ds_list_delete(waitingQueueA, 0);
		else if(ds_list_find_index(waitingQueueB, other) != -1)
			ds_list_delete(waitingQueueB, 0);
		else if(ds_list_find_index(waitingQueueC, other) != -1)
			ds_list_delete(waitingQueueC, 0);
		other.cheated = false;
		other.punished = false;
		other.waitAtServiceMachine = true;
		other.cheatRatio = other.cheatRatio/2;
		ds_list_insert(waitingNumber, 0, other);
	}
}


if(!reneg && !leaving){
	if(beingServed && y > destinationY - 20 && y < destinationY + 20 && x > destinationX - 20 && x < destinationX + 20){
		//PaN
		if(waitAtNumberMachine){
			if(servedTime >= NUMBER_MACHINE_TIME){
				serverID.busy = false;
				waitAtNumberMachine = false;
				servedTime = 0;
				waitingTime = 0;
				beingServed = false;
				
				//placing
				var placeX = random_range(o_WaitingZone.x + sprite_width, o_WaitingZone.x + o_WaitingZone.sprite_width - sprite_width);
				var placeY = random_range(o_WaitingZone.y + sprite_height, o_WaitingZone.y + o_WaitingZone.sprite_height - sprite_height);
				destinationX = placeX;	
				destinationY = placeY;
				
				//add to waiting queue
				with(o_PaN){
					ds_list_add(waitingQueue, other);
					other.wandering = true;
				}
			}
			else
				servedTime++;
		}
		//CT_RT
		if(waitAtServiceMachine){
			if(servedTime >= SERVICE_MACHINE_TIME){
				serverID.busy = false;
				waitAtServiceMachine = false;
				servedTime = 0;
				waitingTime = 0;
				beingServed = false;			
				//add to corresponding waiting queue and check if cheat
				if(serviceTimeWanted <= SERVICE_TIME_SHORT_LIMIT){ //should go to line A 
					with(o_CT_RT){
						var sizeA = ds_list_size(waitingQueueA);
						var sizeB = ds_list_size(waitingQueueB);
						var sizeC = ds_list_size(waitingQueueC);
						if(sizeB < sizeA || sizeC < sizeA){	//there is a shorter queue -> cheat?
							var rand = random_range(0, 99);
							if(sizeB <= sizeC){ //B is shorter or equal
								if((sizeA-sizeB) * other.cheatRatio > rand){
									other.cheated = true;
									ds_list_add(waitingQueueB, other);
								}
							}
							else if(sizeC < sizeB){ //C is shorter
								if((sizeA-sizeC) * other.cheatRatio > rand){
									other.cheated = true;
									ds_list_add(waitingQueueC, other);
								}
							}
							if(!other.cheated) //if he didn't cheat, add to normal queue
								ds_list_add(waitingQueueA, other);			
						} //there is no shorter queue, add to normal queue
						else
							ds_list_add(waitingQueueA, other);
					}
				}
				else if(serviceTimeWanted <= SERVICE_TIME_MEDIUM_LIMIT){
					with(o_CT_RT){
						var sizeA = ds_list_size(waitingQueueA);
						var sizeB = ds_list_size(waitingQueueB);
						var sizeC = ds_list_size(waitingQueueC);
						if(sizeA < sizeB || sizeC < sizeB){	//there is a shorter queue -> cheat?
							var rand = random_range(0, 99);
							if(sizeA <= sizeC){ //A is shorter or equal
								if((sizeB-sizeA) * other.cheatRatio > rand){
									other.cheated = true;
									ds_list_add(waitingQueueA, other);
								}
							}
							else if(sizeC < sizeA){ //C is shorter
								if((sizeB-sizeC) * other.cheatRatio > rand){
									other.cheated = true;
									ds_list_add(waitingQueueC, other);
								}
							}
							if(!other.cheated) //if he didn't cheat, add to normal queue
								ds_list_add(waitingQueueB, other);			
						} //there is no shorter queue, add to normal queue
						else
							ds_list_add(waitingQueueB, other);
					}
				}
				else{
					with(o_CT_RT){
						var sizeA = ds_list_size(waitingQueueA);
						var sizeB = ds_list_size(waitingQueueB);
						var sizeC = ds_list_size(waitingQueueC);
						if(sizeA < sizeC || sizeB < sizeC){	//there is a shorter queue -> cheat?
							var rand = random_range(0, 99);
							if(sizeA <= sizeB){ //A is shorter or equal
								if((sizeC-sizeA) * other.cheatRatio > rand){
									other.cheated = true;
									ds_list_add(waitingQueueA, other);
								}
							}
							else if(sizeB < sizeA){ //B is shorter
								if((sizeC-sizeB) * other.cheatRatio > rand){
									other.cheated = true;
									ds_list_add(waitingQueueB, other);
								}
							}
							if(!other.cheated) //if he didn't cheat, add to normal queue
								ds_list_add(waitingQueueC, other);			
						} //there is no shorter queue, add to normal queue
						else
							ds_list_add(waitingQueueC, other);
					}
				}
			}
			else
				servedTime++;
		}
		if(!waitAtNumberMachine && !waitAtServiceMachine){ //human case
			if(servedTime >= serviceTimeWanted){
				beingServed = false;
				leaving = true;
				serverID.busy = false;
				destinationX = activeModel.exitX
				destinationY = activeModel.exitY + sprite_height;
				with(activeModel)
					servedClients++;		
			}
			servedTime++;
		}
	}	
	//reneging logic
	if(!beingServed && !waitAtNumberMachine && !waitAtServiceMachine){
		with(activeModel){
			//usual case
			if(other.activeModel != o_CT_RT.object_index){
				if(other.waitingTime>other.renegingCap && ds_list_find_index(waitingQueue, other)>=serverAmount){ //reneging ?
					other.reneg = true;
					renegingAmount++;
					//special reneg
					if(other.activeModel == o_PaN.object_index){
						indexWaitReneg = ds_list_find_index(waitingQueue, other);
						ds_list_add(specialRenegs,indexWaitReneg);
					}
					//move others
						for(i = ds_list_find_index(waitingQueue, other) + 1; i<ds_list_size(waitingQueue); i++){
							var toMove = ds_list_find_value(waitingQueue, i);
							if(!toMove.wandering)
								toMove.destinationX = toMove.x - 80;
						}
					//delete from waiting
					ds_list_delete(waitingQueue, ds_list_find_index(waitingQueue, other));
					other.destinationX = entryX;
					other.destinationY = entryY;
				}
			}
			//CT_RT case
			else{
				if(other.serviceTimeWanted <= other.SERVICE_TIME_SHORT_LIMIT){
					if(other.waitingTime>other.renegingCap && ds_list_find_index(waitingQueueA, other)>=serverAmountA){ //reneging ?
						other.reneg = true;
						renegingAmount++;
						//move others
						for(i = ds_list_find_index(waitingQueueA, other) + 1; i<ds_list_size(waitingQueueA); i++){
							var toMove = ds_list_find_value(waitingQueueA, i);
							toMove.destinationY = toMove.y - 50;
						}
						//delete from waiting
						ds_list_delete(waitingQueueA, ds_list_find_index(waitingQueueA, other));
						other.destinationX = entryX;
						other.destinationY = entryY;
					}
				}
				else if(other.serviceTimeWanted <= other.SERVICE_TIME_MEDIUM_LIMIT){
					if(other.waitingTime>other.renegingCap && ds_list_find_index(waitingQueueB, other)>=serverAmountB){ //reneging ?
						other.reneg = true;
						renegingAmount++;
						//move others
						for(i = ds_list_find_index(waitingQueueB, other) + 1; i<ds_list_size(waitingQueueB); i++){
							var toMove = ds_list_find_value(waitingQueueB, i);
							toMove.destinationY = toMove.y - 50;
						}
						//delete from waiting
						ds_list_delete(waitingQueueB, ds_list_find_index(waitingQueueB, other));
						other.destinationX = entryX;
						other.destinationY = entryY;
					}
				}
				else{
					if(other.waitingTime>other.renegingCap && ds_list_find_index(waitingQueueC, other)>=serverAmountC){ //reneging ?
						other.reneg = true;
						renegingAmount++;
						//move others
						for(i = ds_list_find_index(waitingQueueC, other) + 1; i<ds_list_size(waitingQueueC); i++){
							var toMove = ds_list_find_value(waitingQueueC, i);
							toMove.destinationY = toMove.y - 50;
						}
						//delete from waiting
						ds_list_delete(waitingQueueC, ds_list_find_index(waitingQueueC, other));
						other.destinationX = entryX;
						other.destinationY = entryY;
					}
				}				
			}
		}
	}	
	waitingTime++;
}

//death after reneging
if((reneg || leaving) && y > destinationY - 10 && y < destinationY + 10 && x > destinationX - 10 && x < destinationX + 10)	
	instance_destroy();

}