/// @description print balking and reneging
event_inherited();

for(i = 0; i<ds_list_size(servers); i++){
	var serv = ds_list_find_value(servers, i);
	draw_text(serv.x-18, serv.y-60,round((serv.aliveTime-serv.idleTime)/serv.aliveTime*100));	
}


draw_line(entryX - 10, entryY+25, entryX + 30, entryY+25);
draw_line(exitX - 10, exitY+25, exitX + 30, exitY+25);