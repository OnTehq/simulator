/// @description Create a queueing simulator
event_inherited();

waitThreshold = o_Simulator.waitingThreshold;
serversToCreate = o_Simulator.amountOfServ;
serversDelay = o_Simulator.delayToChange;
useThreshold = o_Simulator.usefulThreshold;
maxServers = o_Simulator.maxServerAmount;

aliveTimeCounter = 0;
idleTimeCounter = 0;

serverAmount = serversToCreate
balkingCap = 4*serverAmount;

entryX = 400;
entryY = 600;
exitX = entryX
exitY = y + 50;

servers = ds_list_create();

for(i = 0; i<serverAmount; i++){
	var newServ = instance_create_layer(x+80*(i+1), y+100, "Servers", o_Server);
	newServ.activeModel = object_index;
	ds_list_insert(servers, i, newServ);
}

waitingQueue = ds_list_create();

with(o_Simulator)
	other.nextArrival = ds_list_find_value(arrivalsList, 0);
