/// @description new clients arrive
//show_debug_message(balkingAmount)
if(!o_Simulator.paused){
//checking if server idle is found when people are waiting
if(!ds_list_empty(waitingQueue)){
	idleFound = false;
	for(i = 0; i<ds_list_size(servers) && !idleFound; i++){
		var serv = ds_list_find_value(servers, i);
		if(!serv.busy){
			var newServedClient = ds_list_find_value(waitingQueue, 0);
			newServedClient.beingServed = true;
			newServedClient.serverID = serv.id;
			newServedClient.destinationX = serv.x
			newServedClient.destinationY = serv.y + 50
			serv.busy = true;
			idleFound = true;	
			ds_list_delete(waitingQueue, 0);			
		}			
	}
}

// Arrival && balking logic
if(arrivalCounter>=nextArrival){
	// There is a new client, where to go?
	arrivalCounter = 0;
	arrivedClients++;
	with(o_Simulator)
		other.nextArrival = ds_list_find_value(arrivalsList, other.arrivedClients);
	
	if(ds_list_size(waitingQueue) > balkingCap){	// Balking ?
		if(random(100)>=50){ // Balking prob
			balkingAmount++;
			balk = true;
		}
	}
	if(!balk){
		if(ds_list_empty(waitingQueue)){
			idleFound = false;
			for(i = 0; i<ds_list_size(servers) && !idleFound; i++){
				var serv = ds_list_find_value(servers, i);
				if(!serv.busy){
					var newClient = instance_create_layer(entryX, entryY, "Clients", o_Client);
					newClient.destinationX = serv.x;
					newClient.destinationY = serv.y + 50;
					newClient.beingServed = true;
					newClient.serverID = serv.id;
					newClient.activeModel = object_index;
					serv.busy = true;
					idleFound = true;	
				}			
			}
			if(!idleFound){
				// First waiting client
				var newClient = instance_create_layer(entryX, entryY, "Clients", o_Client);
				newClient.activeModel = object_index;
				ds_list_insert(waitingQueue, 0, newClient)
			}
		}
		else{
			// Locate new client at entry
			var newClient = instance_create_layer(entryX, entryY, "Clients", o_Client);
			newClient.activeModel = object_index;
			ds_list_insert(waitingQueue, ds_list_size(waitingQueue), newClient)
		}
	}
	balk = false;
}
arrivalCounter++;

//update destination of all waiting clients
if(!ds_list_empty(waitingQueue)){
	var currentDirection = 1;
	var newRow = 0;
	var moveFirst = ds_list_find_value(waitingQueue, 0);
	moveFirst.destinationX = ds_list_find_value(servers, 0).x;
	moveFirst.destinationY = ds_list_find_value(servers, 0).y + 160;
	for(i = 1; i<ds_list_size(waitingQueue); i++){
		if(i mod 5 == 0){
			currentDirection = currentDirection * -1;
			newRow = 1;
		}
		var toMove = ds_list_find_value(waitingQueue, i);
		var reference = ds_list_find_value(waitingQueue, i-1);
		if(newRow == 0)
			toMove.destinationX = reference.destinationX + 80 * currentDirection;
		else
			toMove.destinationX = reference.destinationX;
		toMove.destinationY = reference.destinationY + 80*newRow;
		newRow = 0;
	}
}

var allowanceToChange = ((o_Simulator.current_timer/o_Simulator.end_timer*o_Simulator.simTime) mod (serversDelay/60)) == 0;
if(o_Simulator.current_timer == 0)
	allowanceToChange = 0;

if(allowanceToChange){
	//new server
	if(ds_list_size(waitingQueue)> waitThreshold && !recentlyAdded){
		if(!aServerIsQuitting && serverAmount< maxServers){
			serverAmount++;
			balkingCap = 4*serverAmount;
			var newServ = instance_create_layer(x+80*(ds_list_size(servers)+1), y+100, "Servers", o_Server);
			newServ.activeModel = object_index;
			ds_list_insert(servers, ds_list_size(servers), newServ);
			recentlyAdded = true;
			alarm[0] = 60 * o_Simulator.timeRatio * (serversDelay - 1);
		}
	}

	//remove server
	if(serverAmount>1 && !recentlyAdded){
		var removeServer = false;
		var sum = 0;
		for(i = 0; i<ds_list_size(servers); i++){
			var serv = ds_list_find_value(servers, i);
			var servRatio = round((serv.aliveTime-serv.idleTime)/serv.aliveTime*100);
			sum += servRatio;
			if(servRatio > 0 && servRatio < useThreshold)
				removeServer = true;		
		}
		if(sum/ds_list_size(servers) <= (100 - 100/ds_list_size(servers)))
			removeServer = true;
		if(removeServer){
			aServerIsQuitting = true;
			var toDestroy = ds_list_find_value(servers, ds_list_size(servers)-1);
			//ToDestroy
			toDestroy.hasToQuit = true;
		}
	}
}
}