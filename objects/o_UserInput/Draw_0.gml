/// @description --
if(selected)
	image_index = 2;
else if(position_meeting(mouse_x, mouse_y, id))
    image_index = 1;
else
	image_index = 0;
	
draw_self();
draw_set_valign(fa_middle);
draw_text(x, y, text);

draw_set_color(c_white);
switch(typeOfInput){
		case 0 :
			draw_text(x-190, y, "Arrival cats");
			break;
		case 1 :
			draw_text(x-200, y, "Service cats");
			break;
		case 2 :
			draw_text(x-180, y, "Sim time");
			break;
		case 3 :
			draw_text(x-180, y, "Sim dur");
			break;
		case 4:
			draw_text(x-200, y, "Sipp amount");
			break;	
		case 5:
			draw_text(x-200, y, "Initial servers");
			break;	
		case 6:
			draw_text(x-210, y, "Cheating ratio");
			break;	
		case 7:
			draw_text(x-220, y, "Wait threshold");
			break;	
		case 8:
			draw_text(x-230, y, "Delay to change");
			break;	
		case 9:
			draw_text(x-220, y, "Useful treshold");
			break;
		case 10:
			draw_text(x-200, y, "Max Servers");
			break;
}
draw_set_color(c_black);