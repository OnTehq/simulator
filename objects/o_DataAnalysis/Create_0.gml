/// @description --
interrarrivals = ds_list_create();
arrivalCatSize = 0;
arrivalProbs = ds_list_create();
services = ds_list_create();
serviceCatSize = 0;
serviceProbs = ds_list_create();

arrivalCatAmount = "";
serviceCatAmount = "";
simulatedTime = "";	//hours (multiple of 8)
simulationDuration = ""; //minutes(ticks/60)
dailySippAmount = ""; //time division over a day

//read file and assign header values
path = get_open_filename("*.txt","C:\\Users\\gares\\Desktop\\UniFR\\Troisième année\\Travail de bachelor\\Simulator\\mtcl.txt");
f = file_text_open_read(path);

with(o_VariablesBank){
	other.arrivalCatAmount = arrs;
	other.serviceCatAmount = serv;
	other.simulatedTime = stime;
	other.simulationDuration = sdur;
	other.dailySippAmount = sipps;
}

sippIntervalSize = simulatedTime*60/(dailySippAmount+1);
totalSippAmount = dailySippAmount;

//read file and store arrivals and services data
while(!file_text_eof(f)){
	line = file_text_readln(f);
	if(string_char_at(line, 0) != "-"){
		var arrivalTime = "";
		var serviceTime = "";
		var firstPart = true;
		for(i=1; i<string_length(line);i++){
			var c = string_char_at(line, i);
			if(c==",")
				firstPart = false;
			else{
				if(firstPart)
					arrivalTime += c;
				else
				serviceTime += c;
			}
		}
		ds_list_add(interrarrivals, real(arrivalTime));
		ds_list_add(services, real(serviceTime));
	}
}
file_text_close(f);

// Transform absolute arrivals into relative ones (time intervals)
// Also create SIPP list with idexes separating SIPP intervals
sippLocations = ds_list_create();
var sippInterIndex = 0;
var sum = 0;
maxArrival = 0;
minArrival = infinity;
for(i=0;i<ds_list_size(interrarrivals);i++){
	var currentValue = ds_list_find_value(interrarrivals, i);
	if(currentValue > sippIntervalSize*sippInterIndex){
		ds_list_add(sippLocations, i);
		sippInterIndex++;
	}
	var newIntervalTime = currentValue - sum;
	ds_list_replace(interrarrivals, i, newIntervalTime);
	if(newIntervalTime > maxArrival)
		maxArrival = newIntervalTime;
	if(newIntervalTime < minArrival)
		minArrival = newIntervalTime;
	sum += newIntervalTime;
}

arrivalCatSize = (maxArrival-minArrival)/arrivalCatAmount;
averagePerCatArrival = ds_list_create();
for(i = 0; i< arrivalCatAmount; i++){
	ds_list_add(averagePerCatArrival, minArrival+(arrivalCatSize*(i+0.5)));
}

//init probs and frequencies
for(i = 0; i<arrivalCatAmount*(totalSippAmount+1);i++){
	ds_list_add(arrivalProbs, 0);
}
totalFrequencies = ds_list_create();
for(i = 0; i<totalSippAmount+1;i++){
	ds_list_add(totalFrequencies, 0);
}

//let's go for frenquencies
var counter = 1;
for(i = 0; i < ds_list_size(interrarrivals); i++){
	if(i >= ds_list_find_value(sippLocations, counter)){
		counter++;
	}
	var k = floor((ds_list_find_value(interrarrivals, i)-minArrival)/arrivalCatSize);
	if(k==arrivalCatAmount)
		k--;
	var index = k+(counter-1)*arrivalCatAmount;
	ds_list_replace(arrivalProbs, index, ds_list_find_value(arrivalProbs, index) + 1);
	ds_list_replace(totalFrequencies, counter-1, ds_list_find_value(totalFrequencies, counter-1) + 1);
}

//abs prob and cumulative prob
for(i = 0; i< totalSippAmount+1; i++){
	var freqSum = 0;
	for(j=0; j<arrivalCatAmount;j++){
		freqSum += ds_list_find_value(arrivalProbs, i*arrivalCatAmount+j)/ds_list_find_value(totalFrequencies, i);
		ds_list_replace(arrivalProbs, i*arrivalCatAmount+j, freqSum);
	}
}

//look for min and max service and create cat size, then assigne frequencies to cat
//finally, calculate probs
maxService = 0;
minService = infinity;
for(i=0;i<ds_list_size(services);i++){
	if(ds_list_find_value(services, i) > maxService)
		maxService = ds_list_find_value(services, i);
	if(ds_list_find_value(services, i) < minService)
		minService = ds_list_find_value(services, i);
}
serviceCatSize = (maxService - minService)/serviceCatAmount;
averagePerCatService = ds_list_create();
for(i = 0; i< serviceCatAmount; i++){
	ds_list_add(averagePerCatService, minService+(serviceCatSize*(i+0.5)));
}


for(i = 1; i<=serviceCatAmount;i++){
	ds_list_add(serviceProbs, 0);
}
for(i = 0; i<ds_list_size(services);i++){
	var k= floor((ds_list_find_value(services, i)-minService)/serviceCatSize);
	if(k==serviceCatAmount)
		k--;
	ds_list_replace(serviceProbs, k, ds_list_find_value(serviceProbs, k) + 1);
}
var freqSum = 0;
for(i=0; i<ds_list_size(serviceProbs);i++){
	ds_list_replace(serviceProbs, i, ds_list_find_value(serviceProbs, i)/ds_list_size(services) + freqSum);
	freqSum = ds_list_find_value(serviceProbs, i);
}