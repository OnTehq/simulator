/// @description --
/// @description new clients arrive
if(!o_Simulator.paused){
//checking if server idle is found when people are waiting
if(!ds_list_empty(waitingQueueA)){
	idleServerFound = false;
	for(i = 0; i<ds_list_size(serversA) && !idleServerFound; i++){
		var serv = ds_list_find_value(serversA, i);
		if(!serv.busy){
			idleServerFound = true;	
			var newServedClient = ds_list_find_value(waitingQueueA, 0);
			if(newServedClient.cheated && !newServedClient.punished){
				newServedClient.destinationX = serv.x;
				newServedClient.destinationY = serv.y + 50;
				newServedClient.punished = true;		
			}
			else if(newServedClient.punished){
			}
			else{
				newServedClient.beingServed = true;
				newServedClient.serverID = serv.id;
				newServedClient.destinationX = serv.x;
				newServedClient.destinationY = serv.y + 50;
				newServedClient.wandering = false;
				serv.busy = true;
				ds_list_delete(waitingQueueA, 0);
			}
		}			
	}
}
if(!ds_list_empty(waitingQueueB)){
	idleServerFound = false;
	for(i = 0; i<ds_list_size(serversB) && !idleServerFound; i++){
		var serv = ds_list_find_value(serversB, i);
		if(!serv.busy){
			idleServerFound = true;	
			var newServedClient = ds_list_find_value(waitingQueueB, 0);
			if(newServedClient.cheated && !newServedClient.punished){
				newServedClient.destinationX = serv.x;
				newServedClient.destinationY = serv.y + 50;
				newServedClient.punished = true;
			}
			else if(newServedClient.punished){
			}
			else{
				newServedClient.beingServed = true;
				newServedClient.serverID = serv.id;
				newServedClient.destinationX = serv.x
				newServedClient.destinationY = serv.y + 50
				newServedClient.wandering = false;
				serv.busy = true;
				ds_list_delete(waitingQueueB, 0);
			}
		}			
	}
}
if(!ds_list_empty(waitingQueueC)){
	idleServerFound = false;
	for(i = 0; i<ds_list_size(serversC) && !idleServerFound; i++){
		var serv = ds_list_find_value(serversC, i);
		if(!serv.busy){
			idleServerFound = true;	
			var newServedClient = ds_list_find_value(waitingQueueC, 0);
			if(newServedClient.cheated && !newServedClient.punished){
				newServedClient.destinationX = serv.x;
				newServedClient.destinationY = serv.y + 50;
				newServedClient.punished = true;
			}
			else if(newServedClient.punished){
			}
			else{
				newServedClient.beingServed = true;
				newServedClient.serverID = serv.id;
				newServedClient.destinationX = serv.x
				newServedClient.destinationY = serv.y + 50
				newServedClient.wandering = false;
				serv.busy = true;
				ds_list_delete(waitingQueueC, 0);
			}
		}		
	}
}

//cheking if machine idle is found when people are waiting
if(!ds_list_empty(waitingNumber)){
	idleMachineFound = false;
	for(i = 0; i<ds_list_size(machines) && !idleMachineFound; i++){
		var mach = ds_list_find_value(machines, i);
		if(!mach.busy){
			var newServedClient = ds_list_find_value(waitingNumber, 0);
			newServedClient.beingServed = true;
			newServedClient.serverID = mach.id;
			newServedClient.destinationX = mach.x
			newServedClient.destinationY = mach.y + 50
			mach.busy = true;
			idleMachineFound = true;	
			ds_list_delete(waitingNumber, 0);			
		}			
	}
}

// Arrival && balking logic TODO: REWORK BALKING REGARDING OF WANTED SERVICE
if(arrivalCounter>=nextArrival){
	// There is a new client, where to go?
	arrivalCounter = 0;
	arrivedClients++;
	with(o_Simulator)
		other.nextArrival = ds_list_find_value(arrivalsList, other.arrivedClients);
	if(ds_list_size(waitingNumber) + ds_list_size(waitingQueueA) + ds_list_size(waitingQueueB) + ds_list_size(waitingQueueC) > balkingCap){	//balking ?
		if(random(100)>=50){ //balking prob
			balkingAmount++;
			balk = true;
		}
	}
	if(!balk){
		if(ds_list_empty(waitingNumber)){
			idleMachineFound = false;
			for(i = 0; i<ds_list_size(machines) && !idleMachineFound; i++){
				var mach = ds_list_find_value(machines, i);
				if(!mach.busy){
					var newClient = instance_create_layer(entryX, entryY, "Clients", o_Client);
					newClient.destinationX = mach.x;
					newClient.destinationY = mach.y + 50;
					newClient.beingServed = true;
					newClient.serverID = mach.id;
					newClient.activeModel = object_index;
					newClient.waitAtServiceMachine = true;
					mach.busy = true;
					idleMachineFound = true;	
				}			
			}
			if(!idleMachineFound){
				//first waiting client
				var newClient = instance_create_layer(entryX, entryY, "Clients", o_Client);
				newClient.activeModel = object_index;
				newClient.waitAtServiceMachine = true;
				ds_list_insert(waitingNumber, 0, newClient)
			}
		}
		else{
			//locate new client at entry
			var newClient = instance_create_layer(entryX, entryY, "Clients", o_Client);
			newClient.activeModel = object_index;
			newClient.waitAtServiceMachine = true;
			ds_list_insert(waitingNumber, ds_list_size(waitingNumber), newClient)
		}
	}
	balk = false;
}
arrivalCounter++;

//update destination of all waiting clients
if(!ds_list_empty(waitingQueueA)){
	for(i = 0; i<ds_list_size(waitingQueueA); i++){
		var toMove = ds_list_find_value(waitingQueueA, i);
		if(!toMove.punished){
			if(i == 0){
				var serv = ds_list_find_value(serversA, 0);
				var placeX = serv.x
				var placeY = serv.y + 100
			}
			else{
				var reference = ds_list_find_value(waitingQueueA, i);
				var placeX = ds_list_find_value(waitingQueueA, i-1).x;
				var placeY = ds_list_find_value(waitingQueueA, i-1).y + 50;
			}
			toMove.destinationX = placeX;	
			toMove.destinationY = placeY;
		}
	}
}
if(!ds_list_empty(waitingQueueB)){
	for(i = 0; i<ds_list_size(waitingQueueB); i++){
		var toMove = ds_list_find_value(waitingQueueB, i);
		if(!toMove.punished){
			if(i == 0){
				var serv = ds_list_find_value(serversB, 0);
				var placeX = serv.x
				var placeY = serv.y + 100
			}
			else{
				var reference = ds_list_find_value(waitingQueueB, i);
				var placeX = ds_list_find_value(waitingQueueB, i-1).x;
				var placeY = ds_list_find_value(waitingQueueB, i-1).y + 50;
			}
			toMove.destinationX = placeX;	
			toMove.destinationY = placeY;
		}
	}
}
if(!ds_list_empty(waitingQueueC)){
	for(i = 0; i<ds_list_size(waitingQueueC); i++){
		var toMove = ds_list_find_value(waitingQueueC, i);
			if(!toMove.punished){
			if(i == 0){
				var serv = ds_list_find_value(serversC, 0);
				var placeX = serv.x
				var placeY = serv.y + 100
			}
			else{
				var reference = ds_list_find_value(waitingQueueC, i);
				var placeX = ds_list_find_value(waitingQueueC, i-1).x;
				var placeY = ds_list_find_value(waitingQueueC, i-1).y + 50;
			}
			toMove.destinationX = placeX;	
			toMove.destinationY = placeY;
		}
	}
}


if(!ds_list_empty(waitingNumber)){
	var moveFirst = ds_list_find_value(waitingNumber, 0);
	moveFirst.destinationX = ds_list_find_value(machines, 0).x;
	moveFirst.destinationY = ds_list_find_value(machines, 0).y + 160;
	for(i = 1; i<ds_list_size(waitingNumber); i++){
		var toMove = ds_list_find_value(waitingNumber, i);
		var reference = ds_list_find_value(waitingNumber, i-1);
		toMove.destinationX = reference.destinationX + 80;	
	}
}


var allowanceToChange = ((o_Simulator.current_timer/o_Simulator.end_timer*o_Simulator.simTime) mod (serversDelay/60)) == 0;
if(o_Simulator.current_timer == 0)
	allowanceToChange = 0;

if(allowanceToChange){
	//new server
	if(ds_list_size(waitingQueueA)> waitThreshold && !recentlyAddedA){
		if(!aServerIsQuitting && serverAmountA<2 && globalServerAmount<maxServers){
			serverAmountA++;
			balkingCap = 3*(serverAmountA+serverAmountB+serverAmountC);
			globalServerAmount++
			var newServ = instance_create_layer(x+160, y+100, "Servers", o_Server);
			newServ.activeModel = object_index;
			ds_list_insert(serversA, ds_list_size(serversA), newServ);
			recentlyAddedA = true;
			alarm[0] = 60 * o_Simulator.timeRatio * (serversDelay-1);
		}
	}
	if(ds_list_size(waitingQueueB)> waitThreshold && !recentlyAddedB){
		if(!aServerIsQuitting && serverAmountB<2 && globalServerAmount<maxServers){
			serverAmountB++;
			balkingCap = 3*(serverAmountA+serverAmountB+serverAmountC);
			globalServerAmount++
			var newServ = instance_create_layer(x+320, y+100, "Servers", o_Server);
			newServ.activeModel = object_index;
			ds_list_insert(serversB, ds_list_size(serversB), newServ);
			recentlyAddedB = true;
			alarm[1] = 60 * o_Simulator.timeRatio * (serversDelay-1);
		}
	}
	if(ds_list_size(waitingQueueC)> waitThreshold && !recentlyAddedC){
		if(!aServerIsQuitting && serverAmountC<2 && globalServerAmount<maxServers){
			serverAmountC++;
			balkingCap = 3*(serverAmountA+serverAmountB+serverAmountC);
			globalServerAmount++
			var newServ = instance_create_layer(x+480, y+100, "Servers", o_Server);
			newServ.activeModel = object_index;
			ds_list_insert(serversC, ds_list_size(serversC), newServ);
			recentlyAddedC = true;
			alarm[2] = 60 * o_Simulator.timeRatio * (serversDelay-1);
		}
	}

	//remove server
	if(serverAmountA>1 && !recentlyAddedA){
		var removeServer = false;
		var sum = 0;
		for(i = 0; i<ds_list_size(serversA); i++){
			var serv = ds_list_find_value(serversA, i);
			var servRatio = round((serv.aliveTime-serv.idleTime)/serv.aliveTime*100);
			sum += servRatio;
			if(servRatio > 0 && servRatio < useThreshold)
				removeServer = true;		
		}
		if(sum/ds_list_size(serversA) <= (100 - 100/ds_list_size(serversA)))
			removeServer = true;
		if(removeServer){
			globalServerAmount--;
			aServerIsQuitting = true;
			var toDestroy = ds_list_find_value(serversA, ds_list_size(serversA)-1);
			//ToDestroy
			toDestroy.hasToQuitA = true;
		}
	}
	if(serverAmountB>1 && !recentlyAddedB){
		var removeServer = false;
		var sum = 0;
		for(i = 0; i<ds_list_size(serversB); i++){
			var serv = ds_list_find_value(serversB, i);
			var servRatio = round((serv.aliveTime-serv.idleTime)/serv.aliveTime*100);
			sum += servRatio;
			if(servRatio > 0 && servRatio < useThreshold)
				removeServer = true;		
		}
		if(sum/ds_list_size(serversB) <= (100 - 100/ds_list_size(serversB)))
			removeServer = true;
		if(removeServer){
			globalServerAmount--;
			aServerIsQuitting = true;
			var toDestroy = ds_list_find_value(serversB, ds_list_size(serversB)-1);
			//ToDestroy
			toDestroy.hasToQuitB = true;
		}
	}
	if(serverAmountC>1 && !recentlyAddedC){
		var removeServer = false;
		var sum = 0;
		for(i = 0; i<ds_list_size(serversC); i++){
			var serv = ds_list_find_value(serversC, i);
			var servRatio = round((serv.aliveTime-serv.idleTime)/serv.aliveTime*100);
			sum += servRatio;
			if(servRatio > 0 && servRatio < useThreshold)
				removeServer = true;		
		}
		if(sum/ds_list_size(serversC) <= (100 - 100/ds_list_size(serversC)))
			removeServer = true;
		if(removeServer){
			globalServerAmount--;
			aServerIsQuitting = true;
			var toDestroy = ds_list_find_value(serversC, ds_list_size(serversC)-1);
			//ToDestroy
			toDestroy.hasToQuitC = true;
		}
	}
}
}