/// @description Create de PaN model
event_inherited();

waitThreshold = o_Simulator.waitingThreshold;
serversDelay = o_Simulator.delayToChange;
useThreshold = o_Simulator.usefulThreshold;
maxServers = o_Simulator.maxServerAmount;

globalServerAmount = 0
aliveTimeCounter = 0;
idleTimeCounter = 0;

serverAmountA = serversToCreateA
serverAmountB = serversToCreateB
serverAmountC = serversToCreateC

globalServerAmount = serverAmountA + serverAmountB + serverAmountC;

machinesAmount = machinesToCreate
balkingCap = 3*(serverAmountA+serverAmountB+serverAmountC);

entryX = 1600;
entryY = 600;
exitX = entryX + 100;
exitY = y + 50;

serversA = ds_list_create();
serversB = ds_list_create();
serversC = ds_list_create();

for(i = 0; i<serverAmountA; i++){
	var newServ = instance_create_layer(x+80*(i+1), y+100, "Servers", o_Server);
	newServ.activeModel = object_index;
	ds_list_insert(serversA, i, newServ);
}
for(i = 0; i<serverAmountB; i++){
	var newServ = instance_create_layer(x+240 + 80*(i), y+100, "Servers", o_Server);
	newServ.activeModel = object_index;
	ds_list_insert(serversB, i, newServ);
}
for(i = 0; i<serverAmountC; i++){
	var newServ = instance_create_layer(x+400 + 80*(i), y+100, "Servers", o_Server);
	newServ.activeModel = object_index;
	ds_list_insert(serversC, i, newServ);
}

waitingQueueA = ds_list_create();
waitingQueueB = ds_list_create();
waitingQueueC = ds_list_create();

//new

waitingNumber = ds_list_create();


machines = ds_list_create();

for(i = 0; i<machinesAmount; i++){
	ds_list_insert(machines, i, instance_create_layer(x+80*(i+1), y+450, "Servers", o_NumberMachine));
}

with(o_Simulator)
	other.nextArrival = ds_list_find_value(arrivalsList, 0);