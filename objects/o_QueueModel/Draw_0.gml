/// @description --
draw_set_font(fnt_simulatorFont);
draw_self();
draw_rectangle(x, y+670, x+sprite_width, y+680, false);
draw_text(x+50, y+700, "Amount of balking:");
draw_text(x+400, y+700, balkingAmount);
draw_text(x+50, y+750, "Amount of reneging:");
draw_text(x+400, y+750, renegingAmount);
draw_text(x+50, y+800, "Served clients:");
draw_text(x+400, y+800, servedClients);
draw_text(x+50, y+850, "Arrived clients:");
draw_text(x+400, y+850, arrivedClients);
