{
    "id": "5684f86c-0d02-4f9c-ba66-7460e44bfd2e",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_simulatorFontSmall",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "de73ef99-0e61-4158-a910-666ddecbe21d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "e99dfea2-cce7-4c22-88e1-5a4214d22ad2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 73,
                "y": 83
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "50c37fcd-7e34-461e-b450-1201ffa8edf0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 64,
                "y": 83
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "b1a490eb-331b-49bb-85fc-67653b5fe2e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 50,
                "y": 83
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "f9fecfce-8ac7-4079-9397-fb9c6c9d2739",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 37,
                "y": 83
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "d4f799bb-cd66-4b21-b2b8-bea5641893c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 25,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 18,
                "y": 83
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "af1a9f16-5742-4885-bf68-a1021e0c1b3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 2,
                "y": 83
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "b18dc009-794f-4511-92cf-b2c7fb18281a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 25,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 245,
                "y": 56
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "390a9935-8909-4cc4-8100-2e1e46d4e69c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 25,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 237,
                "y": 56
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "61533ec8-479e-4457-8a81-85a35cf901f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 25,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 229,
                "y": 56
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "b1e3b120-5643-4020-9dbe-176811f3e62c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 25,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 79,
                "y": 83
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "353ab577-975d-4f13-bfb1-fa24425db832",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 216,
                "y": 56
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "8542d567-f700-44c3-8b34-290c2035e6c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 198,
                "y": 56
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "22078caa-9909-4817-98af-3c005d45e331",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 189,
                "y": 56
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "0902c1d6-8bdf-417f-a0a4-a033077507c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 183,
                "y": 56
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "848d1343-9b7b-4865-8bfa-44ca844cf235",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 175,
                "y": 56
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "2f9b2daf-9e86-4a31-9fc7-79ec85f8e8bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 162,
                "y": 56
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "eaba298a-91cf-4151-88c1-98f6c45e39bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 25,
                "offset": 2,
                "shift": 12,
                "w": 6,
                "x": 154,
                "y": 56
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "e7cc28e7-0d2c-4dd3-94c8-24238c95a819",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 141,
                "y": 56
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "7ec973a6-0b3a-4758-9d70-56c367e0d9c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 128,
                "y": 56
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "a30c8256-f85e-4209-ad78-ba0f47fb03e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 115,
                "y": 56
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "e8e85ef7-2155-4642-b503-7f5a9fa53fa2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 203,
                "y": 56
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "da22cf0c-b4c5-45f4-9435-75666bb53771",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 89,
                "y": 83
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "c14e1a16-98c1-46d0-bfbc-37b97c03ab05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 102,
                "y": 83
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "2ae6434a-4369-41b3-9ce2-9af0d33c72bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 114,
                "y": 83
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "7427670d-18d4-4f20-8b10-4e18af5e5844",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 150,
                "y": 110
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "f8b93bfb-feaf-4058-9920-a45d8a4649bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 145,
                "y": 110
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "f9fc7862-5b40-4341-a497-fc26bd722d7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 140,
                "y": 110
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "a0b36b13-8bbe-4261-b41e-b88e8467b3f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 127,
                "y": 110
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "44c042d5-9802-4d2a-b95b-a6d84a8491ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 114,
                "y": 110
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "d8577efb-0a89-4faf-927c-5a6392543760",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 101,
                "y": 110
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "971560b0-7afb-4355-9474-14bb63a0cb9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 88,
                "y": 110
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "e2d465fd-b509-4596-b3bb-e5abdc5181a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 25,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 66,
                "y": 110
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "3066ab83-c347-4d44-bebd-08a0574bc448",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 25,
                "offset": -1,
                "shift": 14,
                "w": 16,
                "x": 48,
                "y": 110
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "5bb35b9d-3dae-48f9-bd90-7ca0553e50ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 34,
                "y": 110
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "ca35a7fc-9e90-434b-bb02-f6ac5bda4a68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 18,
                "y": 110
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "d20774f2-78d5-4f7f-ab67-41be97556f16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 2,
                "y": 110
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "c8b20308-3b27-4d9e-9676-c5a2c8f12356",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 232,
                "y": 83
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "5bc8db66-75bc-4ee0-9c7e-e852a4bfe170",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 25,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 219,
                "y": 83
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "0b9aa8c9-0bfe-4184-8f5b-87347c9ed121",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 25,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 202,
                "y": 83
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "c37fa791-4109-48f4-9089-10f6cd99f2e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 187,
                "y": 83
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "5c635bfa-c61b-443a-a77d-561ca07a3c1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 182,
                "y": 83
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "ecc57259-353a-445d-a57e-a699f607eff5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 171,
                "y": 83
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "e91fe21d-0cd7-4452-89b9-301af8ac1dcf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 156,
                "y": 83
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "6e1ed7cd-dab3-41e7-a098-cd6220528250",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 144,
                "y": 83
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "1cfbf54d-7b1f-4117-8a1f-c62cc9c1e90d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 25,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 127,
                "y": 83
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "78474b48-4236-4568-8ce1-45660cd509fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 100,
                "y": 56
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "04dc4266-37cb-4b0c-bfa0-7e3f4b8c23d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 25,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 83,
                "y": 56
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "e531f57f-3287-404d-8649-ef0fbeb87636",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 68,
                "y": 56
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "7ff26f5f-a4dc-4264-b272-957adc800afe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 25,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 40,
                "y": 29
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "701a166a-5822-4dc4-bcb4-680316c9b700",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 17,
                "y": 29
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "1c6e6a9c-52a9-40be-b850-f90f427d406e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 2,
                "y": 29
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "d4d81a5e-03c6-4eec-8023-3206f84f91b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 25,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 232,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "9be8fc67-ca07-47c5-90e9-1d231707606b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 217,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "9354fabd-e110-4b22-9123-c324975ed4d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 201,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "54cb3485-d0ee-4d15-89c2-4d692acce4d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 25,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 179,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "e7c4c04a-4d0d-4acc-8300-d615c4890cf1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 163,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "1a6bc3cc-45de-4cbb-91cf-b052ab6e01ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 147,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "4d8b14c3-24aa-469e-894d-60048b4716e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 25,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 132,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "6aaa1d97-fd7d-4262-b504-d5ec60f643b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 33,
                "y": 29
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "91e4134a-ce5d-4b4c-824a-a086a129b45c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 124,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "1667a3db-f8f6-4665-948c-2f15fecc85c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 108,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "efad7f6a-5dd0-432a-bbe6-77e458dd84a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 25,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 96,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "21d056de-07bd-4cc5-8cfe-0686019ad154",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 25,
                "offset": -1,
                "shift": 12,
                "w": 13,
                "x": 81,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "f2cc7f29-bb90-4d25-87de-19da894b3060",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 74,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "a054001b-f802-4041-a9bf-10ef2c290fff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 61,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "4abea220-0381-40fe-80e8-c0517bdc8148",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 49,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "ffd52461-ee8d-4195-91fc-c768a8ad3111",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 36,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "b30f3817-618e-4c9e-9e5f-66e5ed6cc096",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 23,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "24359c98-de71-43be-995f-260a4ccfafef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 10,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "3d78ee24-c7bc-4cc9-b1f4-caa0e152f1a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 115,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "cde9f7b9-18ff-4aba-98c8-e807a9e9bd84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 58,
                "y": 29
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "ea587157-0358-4018-9ed2-01b4ace38be6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 180,
                "y": 29
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "3e4fbc1e-8fca-4dfe-8bbc-6538d944229c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 25,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 71,
                "y": 29
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "cba19109-f2f1-4bfc-aced-de5de715707e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 25,
                "offset": -1,
                "shift": 5,
                "w": 5,
                "x": 49,
                "y": 56
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "cdcea83f-78f5-42ee-b53f-ba32c1fba0d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 25,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 37,
                "y": 56
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "b73cbb2d-45f9-4182-87c8-bb24750be154",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 25,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 32,
                "y": 56
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "4f7cbc41-5ade-4b9e-a4a2-ed724bd5c72f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 25,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 14,
                "y": 56
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "46a7d812-c6f4-457f-88ca-425fbb6566f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 2,
                "y": 56
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "5b639e80-37a9-4029-bc37-5dcb2d9c9bf3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 234,
                "y": 29
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "80065b89-b883-4010-9399-3cdcc7b2c085",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 222,
                "y": 29
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "834be4bd-9984-4c2d-9fb5-0d4d54a1e190",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 209,
                "y": 29
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "bab2fe35-612c-4d7a-9bca-713a05780171",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 25,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 200,
                "y": 29
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "b4e74a32-d122-4d8d-bb18-6ed7b4a4e134",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 56,
                "y": 56
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "04f93595-9da1-4455-a0de-2e16c4fe9ead",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 192,
                "y": 29
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "49d48649-a598-45bc-a321-fefa4f361199",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 168,
                "y": 29
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "794e75b8-ae2a-4978-88ad-1b8a5e8d61e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 155,
                "y": 29
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "37258083-3be5-4278-8825-cbc2a763bb38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 138,
                "y": 29
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "a65c5eae-fe4d-4762-acbc-ecb1d23020ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 125,
                "y": 29
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "2a0a5c93-ae2c-4c7c-a5bd-dee89d5c494d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 112,
                "y": 29
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "3fa70c0e-40ce-4316-b922-51fe55af0104",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 99,
                "y": 29
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "5aabc37b-c1e8-466c-9bc9-789d07908e26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 90,
                "y": 29
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "d64de32c-a2cc-4e40-8773-a4e9c9d6ef00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 25,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 85,
                "y": 29
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "e00174b4-2780-45ab-910f-0547f328cf26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 76,
                "y": 29
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "2c8f2bb1-2bd9-420c-8a40-31bb8ffb5a9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 163,
                "y": 110
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "5b0eeba7-997c-46be-981d-48a9a40b872b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 25,
                "offset": 4,
                "shift": 20,
                "w": 13,
                "x": 177,
                "y": 110
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "0488006c-41e2-4953-9fe9-d4f4ea0d72b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 65
        },
        {
            "id": "8b3df5fd-ce81-4fc3-8849-beaa97d48c36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 902
        },
        {
            "id": "dff71ab3-434a-4f01-ac4c-2f0fc30bb9f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 913
        },
        {
            "id": "71e6e448-9f24-44a8-b2ef-748d54509326",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 916
        },
        {
            "id": "374b0e80-3a1b-4945-a69c-4acaeb5949fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 923
        },
        {
            "id": "aefb8922-3fe6-4953-b471-e0d64514c735",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 49
        },
        {
            "id": "456e30f1-fbe7-4407-afdd-09b3622b45d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 32
        },
        {
            "id": "9f8f354b-f9a2-4478-bcd4-cd8195d2cbbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "171768b0-55d6-425f-a536-91a9ebc07ecf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "e41ab866-a967-4c0f-b1bf-3e29042ae35a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "7ab2fbce-379d-4e1a-b363-f06ae55b464b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "af82b563-515a-42b7-9da7-e53dbc370eb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 160
        },
        {
            "id": "30228974-4527-4b7c-9010-3dff7050ec50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "ab04b035-5252-469a-8a36-f4af4a837f63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 44
        },
        {
            "id": "b9f19c89-2dcc-49be-a662-9289af8fc209",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 46
        },
        {
            "id": "ad91861c-b86a-4f2c-be9f-4fff8304f0f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "35bced4b-708f-4231-b594-9cadba969006",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 32
        },
        {
            "id": "4ef7477a-8da9-4c0b-8bc3-54c9ab58d446",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "bbaf4c38-3a0b-4d22-ba70-ff3356e42e59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "0be21bf5-b46c-4862-a31f-2a8b9033275b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "fd2d67db-1981-467b-8044-e475d89e0504",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "b8c106c7-e6dd-475a-af02-3b79225365bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "9bf63810-0482-4ea2-a57f-e4d46545a575",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 160
        },
        {
            "id": "14049607-7cc4-48ea-8e53-35f3739a3acf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "4bd95e82-48d9-4f94-8d6a-49c459a32b20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 44
        },
        {
            "id": "f5f28f82-6a90-4862-b9b3-cad841fdaca6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 46
        },
        {
            "id": "d0ba8ea6-427a-4792-8073-7b68744f7821",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "b63f049f-69ff-4ef1-8c06-2e76fdaf638f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 44
        },
        {
            "id": "e1fd3863-ebf6-4280-bf80-f4f19af164c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "95a449b2-1042-43dc-a434-560fb3da9e88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 46
        },
        {
            "id": "38f5f83c-45ba-4936-bd48-94b0ff644df2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 58
        },
        {
            "id": "649de13b-37f9-49c3-8715-c99fa88efcab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 59
        },
        {
            "id": "fe8b65bd-6c09-40da-a38f-9cc3f599c656",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "4c12bd8e-c2c6-448c-9f54-38a66bcf7729",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 97
        },
        {
            "id": "eecebd67-3dd4-4ff3-9520-d37537ce3d99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 99
        },
        {
            "id": "ef8a241c-5c91-4bc7-bb4d-2d30d98269fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 101
        },
        {
            "id": "82e9b725-e5b9-4db0-8054-ecf0c31aff1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 105
        },
        {
            "id": "bfb93b42-aa9b-4dca-9bdc-92f5cace1773",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 111
        },
        {
            "id": "97819832-41fc-4b2a-b5e6-8d82c24604f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "c8634812-0725-4a28-ad4c-e46ad42f4b8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 115
        },
        {
            "id": "4fe735fc-07e2-468a-8170-311546aa2dbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "dc8e401c-24d1-4762-8abf-1bc6f6631057",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "d1ac6953-ad14-46e3-9868-ff6794abcdfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "2dacad56-d535-4879-9e40-8a030fa8e7d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 173
        },
        {
            "id": "2516ab06-5cd6-4c08-9f85-9c88a88f0206",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 894
        },
        {
            "id": "86af6ee7-f427-4eb7-9bab-e4bf24a5f828",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 44
        },
        {
            "id": "2ec79eb9-3bae-441c-8cce-8fe0c88e7d03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "8b322218-27c9-45d3-aa7c-7761984da774",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 46
        },
        {
            "id": "045ea8da-1137-4712-ac89-6611d2868e45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "930b45fd-e71f-45ec-9ba6-76d57f851986",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "c7d9eb16-c9a5-4632-beaa-c519902eabd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "d3e8a6f1-caf6-4edc-bdbc-f2ee7d3a3a0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "192de89b-8df2-45da-a826-a2227bb3fb69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "4c64b734-f789-4ce5-b76a-8858ac893bd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "109974b7-e3f2-4672-a4d4-1c068784138e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "0b948d16-1445-4a3f-8742-65cce9707ef3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "641ce90c-0877-4390-bb11-0e8a2999c0b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 121
        },
        {
            "id": "b6e6e268-f0c9-4eec-bc98-c4735b201d19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 173
        },
        {
            "id": "344525be-c025-4beb-a7c4-691172552716",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 894
        },
        {
            "id": "4f7c5256-a160-4e4c-937f-5cdd1ea5b812",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "a9dcdb7a-4e0e-4687-8d4b-b6cd7e8bcd00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "b9d02fcc-bcfd-481a-bb9f-9b7fca623dcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "36550d09-5ea9-4f4c-98c8-22b18789c0e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "af666d56-c81e-4911-9369-6e24b24852aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 44
        },
        {
            "id": "6dcd6f10-7754-401e-98af-e4b7aaa455f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 45
        },
        {
            "id": "0bf869d9-ed0e-4b81-8bfb-0fa356e4c32b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 46
        },
        {
            "id": "dc43984b-d02f-485d-b33a-716337fa67ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "7dfc8242-7bf4-468c-ab78-521e7e9337a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "9e147935-3d54-4a14-a418-b7860e747e43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "1444247d-7795-4391-8ae4-9f752e211dc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "a9d4f5f7-6882-4a21-b329-4590da794237",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 101
        },
        {
            "id": "70e3aaf8-6543-4222-a434-abce6882a0d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 105
        },
        {
            "id": "7556a62e-a21e-41dd-b62b-f5430f519021",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 111
        },
        {
            "id": "a251fdb9-4911-41fb-ac9e-502b3d521488",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "59a4e3f7-e948-4f79-a8dc-1b5becb049bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 113
        },
        {
            "id": "da2100a0-19e3-4451-8896-c7e990bf2210",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "f8ca18e5-6656-40fd-8ee5-4a4cea7c7887",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "5d806978-5b20-4415-b689-f482f3edb2d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 173
        },
        {
            "id": "e1b244ac-d3d1-4672-b910-057661d60b30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 894
        },
        {
            "id": "58c3e0cd-1e09-4bd3-82d1-ec6cd72ec3ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "f503db9d-6a2d-458a-a4cd-c2d299c15950",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        },
        {
            "id": "f4bfbdbe-bde2-4d6f-ad29-6560824e88ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 8217
        },
        {
            "id": "b94446da-6cdb-44c6-8d78-3817958bc678",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "22a4b68d-f7ca-43de-9d50-515ef89ee82b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "fb1d5f2d-fb9b-4fa0-a924-6f705393cbaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "a554ba10-2c79-47c7-8b2c-5a6d9b0ac2b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "3fb7cd06-1d31-47a8-9932-c2a5a65d7f06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "4666f5b8-8d28-40e8-b608-d01f881904a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 16,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}