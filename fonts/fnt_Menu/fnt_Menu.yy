{
    "id": "586426ec-50e0-4f28-92c7-4ceb85633cef",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_Menu",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 16,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "SF Atarian System",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "7b485c66-8fa5-416c-8c56-10d2fb1c23f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 85,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "16728afe-676c-480b-9de1-08bcc322e3fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 85,
                "offset": 3,
                "shift": 18,
                "w": 11,
                "x": 303,
                "y": 176
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "4658b592-2428-438c-afd7-9681eb1212b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 85,
                "offset": 3,
                "shift": 30,
                "w": 24,
                "x": 277,
                "y": 176
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "34f6002a-8579-4a53-99da-22f4e46048c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 85,
                "offset": 3,
                "shift": 64,
                "w": 57,
                "x": 218,
                "y": 176
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "f75a3fa2-ee09-44a3-b7ad-9c7d51159658",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 85,
                "offset": 3,
                "shift": 36,
                "w": 30,
                "x": 186,
                "y": 176
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "ba4c324c-6cb4-4c29-904e-7884011aa0da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 85,
                "offset": 3,
                "shift": 74,
                "w": 68,
                "x": 116,
                "y": 176
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "231be1f9-30ba-45a8-b407-26dd611514ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 85,
                "offset": 3,
                "shift": 36,
                "w": 30,
                "x": 84,
                "y": 176
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "b35f7eba-5d43-4e54-a7b0-528be777bd85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 85,
                "offset": 3,
                "shift": 18,
                "w": 11,
                "x": 71,
                "y": 176
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "b7274d86-a7d5-491e-a489-a0113217d3ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 85,
                "offset": 3,
                "shift": 26,
                "w": 19,
                "x": 50,
                "y": 176
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "fa14b6f0-dddf-4474-9d9e-e65e089695cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 85,
                "offset": 3,
                "shift": 26,
                "w": 19,
                "x": 29,
                "y": 176
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "10af6107-b9ac-4203-9e89-a49affb505ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 85,
                "offset": 3,
                "shift": 83,
                "w": 77,
                "x": 316,
                "y": 176
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "456b5f70-69ce-42ee-8b06-e0e54d6c3382",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 85,
                "offset": 3,
                "shift": 32,
                "w": 25,
                "x": 2,
                "y": 176
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "bd6b4e9c-235f-4d15-aadb-904a53df4f78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 85,
                "offset": 3,
                "shift": 18,
                "w": 11,
                "x": 969,
                "y": 89
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "52f97f9b-06c5-4cb5-907e-79b56fa54db3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 85,
                "offset": 3,
                "shift": 32,
                "w": 25,
                "x": 942,
                "y": 89
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "f32cd559-10f5-40d0-8eb9-30585d95f9a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 85,
                "offset": 3,
                "shift": 18,
                "w": 11,
                "x": 929,
                "y": 89
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "bbda6a8a-ef5f-48f7-91d1-75fc3cbc6990",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 85,
                "offset": 0,
                "shift": 36,
                "w": 37,
                "x": 890,
                "y": 89
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "5f44e2d5-385a-44eb-bada-ff74204ba6cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 85,
                "offset": 2,
                "shift": 39,
                "w": 36,
                "x": 852,
                "y": 89
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "f652c642-f906-4121-bc93-d017a0d2753e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 85,
                "offset": 3,
                "shift": 18,
                "w": 11,
                "x": 839,
                "y": 89
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "400a3a62-4155-4ce9-bb4b-01f2b20d0521",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 85,
                "offset": 3,
                "shift": 43,
                "w": 37,
                "x": 800,
                "y": 89
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "4f2051da-7e90-4aa1-8feb-73cf71f2ad55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 85,
                "offset": 3,
                "shift": 43,
                "w": 37,
                "x": 761,
                "y": 89
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "0ac587da-92f3-4b04-ae4b-605031e53c83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 85,
                "offset": 3,
                "shift": 45,
                "w": 38,
                "x": 721,
                "y": 89
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "bbc2298a-1956-4aad-9c4c-93573810453e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 85,
                "offset": 3,
                "shift": 43,
                "w": 37,
                "x": 982,
                "y": 89
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "02784d2e-cb27-4508-9cd7-2dbf008e314b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 85,
                "offset": 3,
                "shift": 43,
                "w": 37,
                "x": 395,
                "y": 176
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "f4df8328-d39e-408c-af75-903ff2070dac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 85,
                "offset": 3,
                "shift": 38,
                "w": 34,
                "x": 434,
                "y": 176
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "a237d4fe-a424-4ab8-83a6-f7481aa31f93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 85,
                "offset": 3,
                "shift": 43,
                "w": 37,
                "x": 470,
                "y": 176
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "da2ed9ff-5688-446f-8c51-3a3a4ca407e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 85,
                "offset": 3,
                "shift": 43,
                "w": 37,
                "x": 206,
                "y": 263
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "a90050ed-39ca-4069-a724-af1d38a140c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 85,
                "offset": 3,
                "shift": 18,
                "w": 11,
                "x": 193,
                "y": 263
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "afa3f5fa-2d62-4c29-9ce7-3a60f6c7bc9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 85,
                "offset": 3,
                "shift": 18,
                "w": 11,
                "x": 180,
                "y": 263
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "fde45099-c501-47ee-bd6e-6140a45ad796",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 85,
                "offset": 3,
                "shift": 30,
                "w": 23,
                "x": 155,
                "y": 263
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "0efed15a-d00e-40d9-af52-d4eadec3720d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 85,
                "offset": 3,
                "shift": 40,
                "w": 33,
                "x": 120,
                "y": 263
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "461411e9-fc61-4709-820c-da2fcb8a4e13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 85,
                "offset": 3,
                "shift": 30,
                "w": 23,
                "x": 95,
                "y": 263
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "3ef7f6f7-a885-44f2-b82b-02140cd7c663",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 85,
                "offset": 3,
                "shift": 43,
                "w": 37,
                "x": 56,
                "y": 263
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "d9293eb7-1f97-4ee2-9da3-b0cd9234ea13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 85,
                "offset": 2,
                "shift": 57,
                "w": 52,
                "x": 2,
                "y": 263
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "5c86bb9e-60ef-4698-b298-c9059697ba00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 85,
                "offset": -2,
                "shift": 42,
                "w": 47,
                "x": 960,
                "y": 176
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "b8ce46be-9d13-4507-a124-e7ed69e56c08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 85,
                "offset": 3,
                "shift": 45,
                "w": 41,
                "x": 917,
                "y": 176
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "2ae20510-2d44-4d98-a1e8-8b0339a352aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 85,
                "offset": 3,
                "shift": 43,
                "w": 37,
                "x": 878,
                "y": 176
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "4b372b35-1c1e-49d8-b5e2-23dd00b662ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 85,
                "offset": 3,
                "shift": 43,
                "w": 37,
                "x": 839,
                "y": 176
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "3c39b04e-d0c8-4f9b-859d-66f23c0db4f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 85,
                "offset": 3,
                "shift": 39,
                "w": 33,
                "x": 804,
                "y": 176
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "8383c1e2-b450-4d54-8b7a-c7c9f834e8d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 85,
                "offset": 3,
                "shift": 37,
                "w": 33,
                "x": 769,
                "y": 176
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "213312fe-5d9c-4072-bf11-5f8d5d5ae3b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 85,
                "offset": 3,
                "shift": 43,
                "w": 37,
                "x": 730,
                "y": 176
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "eba41fad-e7a1-4853-b921-af418b2ea957",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 85,
                "offset": 3,
                "shift": 43,
                "w": 37,
                "x": 691,
                "y": 176
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "b5e934e8-a73f-465f-9bef-ef2fb0dc91af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 85,
                "offset": 3,
                "shift": 18,
                "w": 11,
                "x": 678,
                "y": 176
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "586f19df-d2ec-41d5-a2c8-eca082db1d50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 85,
                "offset": 2,
                "shift": 41,
                "w": 36,
                "x": 640,
                "y": 176
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "50ed6e62-8a11-4387-8fd8-d97989770efb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 85,
                "offset": 3,
                "shift": 41,
                "w": 37,
                "x": 601,
                "y": 176
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "74f2433f-cf69-4c22-820c-611331b3c467",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 85,
                "offset": 3,
                "shift": 37,
                "w": 33,
                "x": 566,
                "y": 176
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "9b12d9b1-bf59-4053-a1eb-77bb59e51fa5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 85,
                "offset": 3,
                "shift": 62,
                "w": 55,
                "x": 509,
                "y": 176
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "c9c6358c-b3fa-4680-a1f1-fa67d57c2597",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 85,
                "offset": 3,
                "shift": 43,
                "w": 37,
                "x": 682,
                "y": 89
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "92a38f65-ec0e-4312-9454-4034649be2f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 85,
                "offset": 2,
                "shift": 39,
                "w": 36,
                "x": 644,
                "y": 89
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "a03fdff6-9008-47ce-b32f-eb18710c9f99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 85,
                "offset": 3,
                "shift": 45,
                "w": 38,
                "x": 604,
                "y": 89
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "73094bf0-7b37-44e4-8f58-fc7eb3e1534c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 85,
                "offset": 2,
                "shift": 41,
                "w": 37,
                "x": 837,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "a2de57a6-a91f-4209-ab04-5f7714b51da0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 85,
                "offset": 3,
                "shift": 44,
                "w": 40,
                "x": 774,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "8a9e84e6-8e42-4680-b247-ab944234e91d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 85,
                "offset": 3,
                "shift": 43,
                "w": 37,
                "x": 735,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "83a80d9c-b510-4f61-a9a7-0c89d202fd63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 85,
                "offset": 0,
                "shift": 37,
                "w": 36,
                "x": 697,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "6afe960f-1755-4890-b2ae-7bb2ec4c6e71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 85,
                "offset": 3,
                "shift": 43,
                "w": 37,
                "x": 658,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "63abca28-902d-4c4d-b30e-bdaf71d7d8fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 85,
                "offset": -2,
                "shift": 42,
                "w": 47,
                "x": 609,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "a6c0492c-1b88-4e57-9dcf-e6dd281f2ab6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 85,
                "offset": -2,
                "shift": 72,
                "w": 76,
                "x": 531,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "40e37165-cf3c-41e2-a91c-571ac5685b58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 85,
                "offset": 0,
                "shift": 43,
                "w": 44,
                "x": 485,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "ae3fa62f-7d02-429e-845c-e403cb393894",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 85,
                "offset": -2,
                "shift": 39,
                "w": 44,
                "x": 439,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "f4a0f8a6-9eca-4e11-a2d8-27f7ab7ca4b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 85,
                "offset": 3,
                "shift": 43,
                "w": 37,
                "x": 400,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "85b085a2-e29d-492e-869c-8b1f1bfb599b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 85,
                "offset": 3,
                "shift": 26,
                "w": 19,
                "x": 816,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "690323f8-2089-442e-9c9e-f6dd70a78155",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 85,
                "offset": 0,
                "shift": 36,
                "w": 37,
                "x": 361,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "773c3aa7-d6a9-47cf-83e3-427427e670bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 85,
                "offset": 3,
                "shift": 26,
                "w": 19,
                "x": 312,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "0e2ca9e9-2bc0-4a98-84d5-7b9841cb202a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 85,
                "offset": 5,
                "shift": 58,
                "w": 48,
                "x": 262,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "6e2f75d0-ea00-4ebe-afe8-30184c13d914",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 85,
                "offset": -2,
                "shift": 40,
                "w": 44,
                "x": 216,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "eebb1265-ce1b-4235-9f3a-b8d118403208",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 85,
                "offset": 19,
                "shift": 58,
                "w": 20,
                "x": 194,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "9a137aae-b556-4776-9629-1b568b8d678a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 85,
                "offset": -2,
                "shift": 33,
                "w": 37,
                "x": 155,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "22896a87-3c87-4b40-894c-ec73d09d480f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 85,
                "offset": 3,
                "shift": 37,
                "w": 33,
                "x": 120,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "fbdcecc4-a97e-4ba5-99ca-b9769fa14328",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 85,
                "offset": 3,
                "shift": 36,
                "w": 30,
                "x": 88,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "bb04f165-3ad5-44b7-8896-d733198051c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 85,
                "offset": 3,
                "shift": 36,
                "w": 30,
                "x": 56,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "c883effe-d849-446a-ac70-1abbae1e3f7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 85,
                "offset": 3,
                "shift": 33,
                "w": 26,
                "x": 28,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "7388fc97-d8d3-485c-b472-b8a94019b3ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 85,
                "offset": 3,
                "shift": 31,
                "w": 26,
                "x": 333,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "6820db30-3133-44c3-972c-7b18bfd15d00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 85,
                "offset": 3,
                "shift": 36,
                "w": 30,
                "x": 876,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "77abf171-feac-432e-9fd4-0dd0b1e8b5b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 85,
                "offset": 3,
                "shift": 36,
                "w": 30,
                "x": 210,
                "y": 89
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "488c5ec5-b9ac-49f8-aeb5-99faac945e9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 85,
                "offset": 3,
                "shift": 16,
                "w": 9,
                "x": 908,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "e2abecd2-0524-4fef-a0d1-b3f7551514e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 85,
                "offset": 2,
                "shift": 34,
                "w": 29,
                "x": 541,
                "y": 89
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "084473eb-685a-41ad-8fec-e4d85eb2ad81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 85,
                "offset": 3,
                "shift": 34,
                "w": 30,
                "x": 509,
                "y": 89
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "83919df2-1fff-4383-ba5d-794bc9c071ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 85,
                "offset": 3,
                "shift": 31,
                "w": 26,
                "x": 481,
                "y": 89
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "ef198f1e-1836-4c59-8a49-cb7a09fa3169",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 85,
                "offset": 3,
                "shift": 51,
                "w": 44,
                "x": 435,
                "y": 89
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "511dd59f-de99-4162-88cb-e7cba54284c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 85,
                "offset": 3,
                "shift": 36,
                "w": 30,
                "x": 403,
                "y": 89
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "3ffcf41a-f805-4ebe-ba1b-49db9eb60f52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 85,
                "offset": 2,
                "shift": 32,
                "w": 29,
                "x": 372,
                "y": 89
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "2279b08a-2de1-4054-b45b-5a6890655a70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 85,
                "offset": 3,
                "shift": 35,
                "w": 31,
                "x": 339,
                "y": 89
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "31737806-a2d7-4542-8801-65f614bb2c64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 85,
                "offset": 2,
                "shift": 33,
                "w": 30,
                "x": 307,
                "y": 89
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "256e40af-e755-4136-8d75-5afc2bdab51c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 85,
                "offset": 3,
                "shift": 37,
                "w": 32,
                "x": 273,
                "y": 89
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "e11ea8d2-5766-4e16-90f9-fb2c05adb4e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 85,
                "offset": 3,
                "shift": 36,
                "w": 30,
                "x": 572,
                "y": 89
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "8a214182-8178-4aa0-880d-1a0d52e851e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 85,
                "offset": 0,
                "shift": 30,
                "w": 29,
                "x": 242,
                "y": 89
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "58a58f1d-e267-40c5-8c07-4e5b5260c948",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 85,
                "offset": 3,
                "shift": 36,
                "w": 30,
                "x": 178,
                "y": 89
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "5f320463-9af6-497b-a9a4-42a06f87e401",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 85,
                "offset": -2,
                "shift": 33,
                "w": 37,
                "x": 139,
                "y": 89
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "a827c9af-0763-4b60-99a7-cb9abc411a74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 85,
                "offset": -2,
                "shift": 56,
                "w": 61,
                "x": 76,
                "y": 89
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "1e16e4f0-79e6-45c8-8dd3-e16051e69243",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 85,
                "offset": 0,
                "shift": 35,
                "w": 35,
                "x": 39,
                "y": 89
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "eedb3b63-4805-4156-bf0d-19b9b8c30cd0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 85,
                "offset": -2,
                "shift": 31,
                "w": 35,
                "x": 2,
                "y": 89
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "bf61cd1c-b772-47dc-8f67-be494dcb9027",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 85,
                "offset": 3,
                "shift": 36,
                "w": 30,
                "x": 974,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "9ad1cbd4-9a27-4d78-a2b8-78f207dafa8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 85,
                "offset": 3,
                "shift": 26,
                "w": 19,
                "x": 953,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "d17cdd6b-cec9-4b23-9b05-7f47dfb807d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 85,
                "offset": 3,
                "shift": 18,
                "w": 11,
                "x": 940,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "6ddb3c5b-31b7-470a-9aee-29c999ebaa48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 85,
                "offset": 3,
                "shift": 26,
                "w": 19,
                "x": 919,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "0951c2d4-1872-41dc-9ebc-c2ee84ecdc79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 85,
                "offset": 7,
                "shift": 58,
                "w": 44,
                "x": 245,
                "y": 263
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "ef179a39-e86b-4a53-91c8-89a96fff0f18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 85,
                "offset": 19,
                "shift": 93,
                "w": 55,
                "x": 291,
                "y": 263
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "aca69995-0b61-485f-9ef5-d63f0057d039",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 65,
            "second": 84
        },
        {
            "id": "eab8d504-9a1e-461e-b232-add7f27cf83a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 65,
            "second": 116
        },
        {
            "id": "ee5d1b7c-d620-43cc-bac4-5e624293ae68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 84,
            "second": 65
        },
        {
            "id": "c4d41a5c-3e84-491f-aeaa-fa0696be563b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 84,
            "second": 97
        },
        {
            "id": "4720131f-4928-4cd6-a08e-0789101bbd02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 97,
            "second": 84
        },
        {
            "id": "40763d90-8f94-4050-9820-d014e00c48a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 97,
            "second": 116
        },
        {
            "id": "b7ff07af-2d8d-47ec-939e-d22e90a41021",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 116,
            "second": 65
        },
        {
            "id": "5f4911be-1414-4ffe-8a99-c6320051e4af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 116,
            "second": 97
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 72,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}