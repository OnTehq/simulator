{
    "id": "586426ec-50e0-4f28-92c7-4ceb85633cef",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_Menu",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 12,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "SF Atarian System",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "24a06bd7-9362-4c37-ac0b-eabf25aeef9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 57,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "1e5dde81-23d2-4ef3-8a2d-7336e0e30297",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 57,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 129,
                "y": 179
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "3f4e1b51-6a34-44d9-9bc1-297fb506557d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 57,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 111,
                "y": 179
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "b256b49a-d0a9-4f3b-b345-7f6895f60362",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 57,
                "offset": 2,
                "shift": 43,
                "w": 38,
                "x": 71,
                "y": 179
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "e4cdb513-85f3-42a4-9227-1836941f1a3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 57,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 49,
                "y": 179
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "43929377-47b6-4690-9e36-c6d19f6b7fdb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 57,
                "offset": 2,
                "shift": 49,
                "w": 45,
                "x": 2,
                "y": 179
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "7b26ef92-0387-4118-8af7-9d3fa4d4c92a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 57,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 471,
                "y": 120
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "cf10b0f5-ea06-47d1-8a22-d2023c51e278",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 57,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 461,
                "y": 120
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "e61acc32-5e6b-4b84-86fd-05cb458323d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 57,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 446,
                "y": 120
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "4c48792a-f488-4045-bb7e-0990bbdecaa1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 57,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 431,
                "y": 120
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "5c552765-b3da-4a39-86d6-90d4d108ffba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 57,
                "offset": 2,
                "shift": 55,
                "w": 51,
                "x": 139,
                "y": 179
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "661b25c9-0c3f-4319-8965-554a46864182",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 57,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 412,
                "y": 120
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "718fcbef-ef5f-40dd-b79d-398cf095b8a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 57,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 375,
                "y": 120
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "0f12d841-5952-42e3-bae1-b94d25ead6e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 57,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 356,
                "y": 120
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "650ac2c0-6968-4f54-815b-98391f014f71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 57,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 346,
                "y": 120
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "88cf8e25-9be6-4cfe-b009-02089ad68753",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 57,
                "offset": 0,
                "shift": 24,
                "w": 25,
                "x": 319,
                "y": 120
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "8090f214-1858-4ab2-b148-917930214255",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 57,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 293,
                "y": 120
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "d72065b3-1d57-4ef1-a39a-3d1ae57f23af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 57,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 283,
                "y": 120
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "53221d86-077c-4935-bba3-4b312442b4b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 57,
                "offset": 2,
                "shift": 29,
                "w": 25,
                "x": 256,
                "y": 120
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "3e884964-e98f-4aa3-89cc-0c28b9e0744e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 57,
                "offset": 2,
                "shift": 29,
                "w": 25,
                "x": 229,
                "y": 120
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "b4cf53d8-a47d-4c9c-9783-7e3a789a2292",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 57,
                "offset": 2,
                "shift": 30,
                "w": 26,
                "x": 201,
                "y": 120
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "58ea0be2-4247-4df4-b619-d542735f2f56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 57,
                "offset": 2,
                "shift": 29,
                "w": 25,
                "x": 385,
                "y": 120
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "6728c685-5550-476a-8c41-51dc886ce8fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 57,
                "offset": 2,
                "shift": 29,
                "w": 25,
                "x": 192,
                "y": 179
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "76859f4a-cc70-40f6-8b7a-bc9bd5c70c38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 57,
                "offset": 2,
                "shift": 26,
                "w": 23,
                "x": 219,
                "y": 179
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "9d03c71a-f7ab-48c9-b0df-548d83a3a842",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 57,
                "offset": 2,
                "shift": 29,
                "w": 25,
                "x": 244,
                "y": 179
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "17757b25-abbc-4bc9-8b91-80f4b8b10053",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 57,
                "offset": 2,
                "shift": 29,
                "w": 25,
                "x": 263,
                "y": 238
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "3fa22558-94d6-493d-a53a-dfc083975144",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 57,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 253,
                "y": 238
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "97f5abba-6c67-4cec-b6c8-9208ac62dc0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 57,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 243,
                "y": 238
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "bbc365a1-dcec-48f9-b59d-5ef8c64dc861",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 57,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 225,
                "y": 238
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "c958c300-d471-478a-af04-ca0559a1a1f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 57,
                "offset": 2,
                "shift": 27,
                "w": 22,
                "x": 201,
                "y": 238
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "fb8d2c0a-6762-4f4b-8d9d-aacc8e75060e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 57,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 183,
                "y": 238
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "2667c2f8-35c0-46b9-9173-c4cc9ac0f000",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 57,
                "offset": 2,
                "shift": 29,
                "w": 25,
                "x": 156,
                "y": 238
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "1381d900-4ee1-4005-af5e-40204253d00b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 57,
                "offset": 1,
                "shift": 38,
                "w": 35,
                "x": 119,
                "y": 238
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "4913e3f4-ff82-4e54-838d-866048bbacec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 57,
                "offset": -2,
                "shift": 28,
                "w": 32,
                "x": 85,
                "y": 238
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "ffb7a53f-b454-4021-8c5c-54beeafaafbd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 57,
                "offset": 2,
                "shift": 30,
                "w": 27,
                "x": 56,
                "y": 238
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "a9d8d0fc-899e-4f43-b248-f27d0cd44966",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 57,
                "offset": 2,
                "shift": 29,
                "w": 25,
                "x": 29,
                "y": 238
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "36c1a957-c3cf-462d-ab84-7de1ece7bcc0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 57,
                "offset": 2,
                "shift": 29,
                "w": 25,
                "x": 2,
                "y": 238
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "96d0d309-52c7-49d5-a7e7-b2f4cb10542f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 57,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 475,
                "y": 179
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "97da78a8-6904-4b94-ae5f-bdfb1da8595a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 57,
                "offset": 2,
                "shift": 25,
                "w": 22,
                "x": 451,
                "y": 179
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "f57c1ce2-f0d6-4214-871d-d054cfff6883",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 57,
                "offset": 2,
                "shift": 29,
                "w": 25,
                "x": 424,
                "y": 179
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "bd3a4c34-4c05-4dca-bd60-345a33c18a6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 57,
                "offset": 2,
                "shift": 29,
                "w": 25,
                "x": 397,
                "y": 179
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "5a987679-fff6-4870-a207-7d035be63610",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 57,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 387,
                "y": 179
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "9f6e4ff4-adea-4914-8799-8fdebddfa640",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 57,
                "offset": 1,
                "shift": 27,
                "w": 24,
                "x": 361,
                "y": 179
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "8502f59b-b8f8-4abb-b2ec-62c9349c4d42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 57,
                "offset": 2,
                "shift": 27,
                "w": 25,
                "x": 334,
                "y": 179
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "59e0f7e6-8be8-4b17-b71b-c19505120435",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 57,
                "offset": 2,
                "shift": 25,
                "w": 22,
                "x": 310,
                "y": 179
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "f288ff52-3f2c-4f91-9576-b1d7ea771661",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 57,
                "offset": 2,
                "shift": 41,
                "w": 37,
                "x": 271,
                "y": 179
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "2588504d-c33f-4630-ae37-1060312646c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 57,
                "offset": 2,
                "shift": 29,
                "w": 25,
                "x": 174,
                "y": 120
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "2a5dc603-a314-4ead-b86e-82e0cd7d36be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 57,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 148,
                "y": 120
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "01c35a11-b442-4d62-84b7-728cae59ace9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 57,
                "offset": 2,
                "shift": 30,
                "w": 26,
                "x": 120,
                "y": 120
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "2b384762-0d75-4743-8b12-9582b9cd7c95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 57,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 73,
                "y": 61
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "b3c8a917-10f4-4f3a-8a0c-11f76fa57fe0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 57,
                "offset": 2,
                "shift": 30,
                "w": 27,
                "x": 29,
                "y": 61
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "6fad0014-72cb-4d82-b718-d0318896f9b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 57,
                "offset": 2,
                "shift": 29,
                "w": 25,
                "x": 2,
                "y": 61
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "f6884631-b000-48f5-896a-fccedb689e6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 57,
                "offset": 0,
                "shift": 25,
                "w": 24,
                "x": 483,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "52748e72-387d-472a-9c11-35a4005431ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 57,
                "offset": 2,
                "shift": 29,
                "w": 25,
                "x": 456,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "81221349-07d5-48bf-8a3e-caa1c793ba21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 57,
                "offset": -2,
                "shift": 28,
                "w": 32,
                "x": 422,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "d847c8bc-ab93-457a-b753-a1b5bd325a34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 57,
                "offset": -2,
                "shift": 48,
                "w": 52,
                "x": 368,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "5faff850-7906-43a6-b1a0-d0815286881f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 57,
                "offset": 0,
                "shift": 29,
                "w": 29,
                "x": 337,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "558c15ff-3e3c-4726-9bdd-769a849ba0d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 57,
                "offset": -2,
                "shift": 26,
                "w": 30,
                "x": 305,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "41083e54-c95c-4094-bbd3-e25558e615ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 57,
                "offset": 2,
                "shift": 29,
                "w": 25,
                "x": 278,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "e99e5503-a261-4631-9896-442af1318a8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 57,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 58,
                "y": 61
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "44a01226-948a-46c3-af61-889ad6fae54d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 57,
                "offset": 0,
                "shift": 24,
                "w": 25,
                "x": 251,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "92f59db9-4f05-474d-8755-85df3af3a32a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 57,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 216,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "49ce1ff4-8124-4486-bb34-a1483581b560",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 57,
                "offset": 3,
                "shift": 38,
                "w": 32,
                "x": 182,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "dcf71083-3b9e-45f7-ab56-7cf21eb6a17c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 57,
                "offset": -1,
                "shift": 27,
                "w": 29,
                "x": 151,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "d32d9e42-dd83-433d-af35-cf9f53316d52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 57,
                "offset": 13,
                "shift": 38,
                "w": 13,
                "x": 136,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "372d7868-c36e-465a-adc4-584b28ba8a16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 57,
                "offset": -2,
                "shift": 22,
                "w": 26,
                "x": 108,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "be5e996d-5222-4a03-8ff5-c79d9d7a2993",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 57,
                "offset": 2,
                "shift": 25,
                "w": 22,
                "x": 84,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "e5e7a52a-3464-4b99-b650-15588de8073e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 57,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 62,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "abc3445b-618e-4dc5-a983-87573bee5bb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 57,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 40,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "4fc17f2e-54c1-4a4c-8c9a-c87ebba50b37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 57,
                "offset": 2,
                "shift": 22,
                "w": 18,
                "x": 20,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "140dd46e-0647-467c-8f27-c746cca63035",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 57,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 231,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "0f44621a-eba6-40b1-8858-38ac08108cde",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 57,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 100,
                "y": 61
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "b7f5eb21-3715-4e15-b6e1-58a36c39b9a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 57,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 337,
                "y": 61
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "f43f5dc2-4dd0-46e7-8253-471b063f1c10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 57,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 122,
                "y": 61
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "02d4a839-8ba3-4c8b-a6e9-18cf56ac743e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 57,
                "offset": 1,
                "shift": 23,
                "w": 20,
                "x": 76,
                "y": 120
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "13c70437-7f94-4051-ba81-c80f9e1e2355",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 57,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 54,
                "y": 120
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "988982c7-4737-4097-8823-5468f2387be8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 57,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 34,
                "y": 120
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "2a44136a-8e79-46c7-9f2b-026a7aa2c611",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 57,
                "offset": 2,
                "shift": 34,
                "w": 30,
                "x": 2,
                "y": 120
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "b47c336e-43ca-427e-97e2-4e79357f704d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 57,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 472,
                "y": 61
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "caa0f685-8ae7-4876-9fd8-7488235f4989",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 57,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 450,
                "y": 61
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "0eb7c7f3-35ca-4c33-b87e-a8bc1c4952fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 57,
                "offset": 2,
                "shift": 24,
                "w": 21,
                "x": 427,
                "y": 61
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "3adb83bf-0b75-4156-b88b-73e9277ef9dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 57,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 405,
                "y": 61
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "f21b20c8-bd1f-4091-9515-1bef404895ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 57,
                "offset": 2,
                "shift": 24,
                "w": 22,
                "x": 381,
                "y": 61
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "5aa5cde8-2145-4c72-bf99-f84a033128ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 57,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 98,
                "y": 120
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "95639f07-7452-4afd-8632-4639824fc401",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 57,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 359,
                "y": 61
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "fa1894bb-8d37-47d4-9073-0a18c203a51c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 57,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 315,
                "y": 61
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "070beb94-5d3e-436e-b9fd-1852bc794404",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 57,
                "offset": -2,
                "shift": 22,
                "w": 26,
                "x": 287,
                "y": 61
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "44b317cd-89ec-42ef-ad70-61fd3b8ad85f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 57,
                "offset": -2,
                "shift": 38,
                "w": 41,
                "x": 244,
                "y": 61
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "b0cd8316-19e8-4f09-b747-7555df1f7759",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 57,
                "offset": 0,
                "shift": 23,
                "w": 24,
                "x": 218,
                "y": 61
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "117c0feb-2e03-4628-b80f-5d1b703af6f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 57,
                "offset": -2,
                "shift": 20,
                "w": 24,
                "x": 192,
                "y": 61
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "d6b1d6b0-9735-40e8-ad5f-01bca97b2fa4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 57,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 170,
                "y": 61
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "b6714ae2-0088-41dc-8e8a-29b1758f836e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 57,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 155,
                "y": 61
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "70b654b2-b7bf-4b01-9880-524d4ede6995",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 57,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 145,
                "y": 61
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "ba36dee2-41c2-449c-82f3-5b3242d219ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 57,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 130,
                "y": 61
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "27086401-314a-4828-bebc-4a6ea32208ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 57,
                "offset": 4,
                "shift": 38,
                "w": 30,
                "x": 290,
                "y": 238
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "5b8e8286-11e0-4f0e-b225-f0cf3444a52f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 57,
                "offset": 12,
                "shift": 62,
                "w": 38,
                "x": 322,
                "y": 238
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "2a676195-75cf-446a-9610-a304de56a7e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 65,
            "second": 84
        },
        {
            "id": "adad40f9-e33e-4068-986e-a91b18c9108b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 116
        },
        {
            "id": "07c63f60-772a-4c95-b54f-50a37425397b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 65
        },
        {
            "id": "230809f4-f49e-4094-8a03-7302a09e02b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 97
        },
        {
            "id": "a959304c-da4e-45fd-af43-115f7ce6c66c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 97,
            "second": 84
        },
        {
            "id": "25c6b3e1-899d-49e1-82c4-40d8c6927fb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 97,
            "second": 116
        },
        {
            "id": "03b4aecd-14bf-4822-ac33-3ae971787604",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 116,
            "second": 65
        },
        {
            "id": "61667031-761f-48cb-9d65-fddadc8e5f5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 116,
            "second": 97
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 48,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}