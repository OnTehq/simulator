{
    "id": "06e7d047-36c2-4364-b537-16cb4140ad95",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_Menu_small",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 16,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "SF Atarian System",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "38246f23-b946-4860-9e44-3062c6174835",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 85,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "eacb5fdb-093f-4462-ab32-96ae248d7c98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 85,
                "offset": 3,
                "shift": 18,
                "w": 11,
                "x": 303,
                "y": 176
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "d1f59b48-3c2d-40a3-8cc8-2394b3af88e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 85,
                "offset": 3,
                "shift": 30,
                "w": 24,
                "x": 277,
                "y": 176
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "5e528274-76a6-4f92-9cb2-683d5ec96d2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 85,
                "offset": 3,
                "shift": 64,
                "w": 57,
                "x": 218,
                "y": 176
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "3b1ae124-eafe-4423-8a5b-d75bc0345536",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 85,
                "offset": 3,
                "shift": 36,
                "w": 30,
                "x": 186,
                "y": 176
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "59d3ff04-0381-4d53-8a6b-521ebf404399",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 85,
                "offset": 3,
                "shift": 74,
                "w": 68,
                "x": 116,
                "y": 176
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "1bd04cc6-7d54-40a0-9389-d784c3389443",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 85,
                "offset": 3,
                "shift": 36,
                "w": 30,
                "x": 84,
                "y": 176
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "d97f9a84-a0e6-465d-904a-e6459826f824",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 85,
                "offset": 3,
                "shift": 18,
                "w": 11,
                "x": 71,
                "y": 176
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "3f0be4e6-fadc-485b-8379-c567a4ea75f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 85,
                "offset": 3,
                "shift": 26,
                "w": 19,
                "x": 50,
                "y": 176
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "6b21c6fc-4901-4698-9826-6df9edb0e4f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 85,
                "offset": 3,
                "shift": 26,
                "w": 19,
                "x": 29,
                "y": 176
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "118d5a2b-22b3-4da7-9ca6-58da75993aa8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 85,
                "offset": 3,
                "shift": 83,
                "w": 77,
                "x": 316,
                "y": 176
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "ff86f3de-42fb-4fbc-a39e-9ebf7354c2ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 85,
                "offset": 3,
                "shift": 32,
                "w": 25,
                "x": 2,
                "y": 176
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "84549574-524a-4792-838d-d61f788e0ee0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 85,
                "offset": 3,
                "shift": 18,
                "w": 11,
                "x": 969,
                "y": 89
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "9a3a9323-c090-4780-a7b8-7b7649ba8946",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 85,
                "offset": 3,
                "shift": 32,
                "w": 25,
                "x": 942,
                "y": 89
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "e840246a-333a-4b0c-ae3b-4985a8bc1a5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 85,
                "offset": 3,
                "shift": 18,
                "w": 11,
                "x": 929,
                "y": 89
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "114a1345-e9b4-48c2-91a5-3150cd95d790",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 85,
                "offset": 0,
                "shift": 36,
                "w": 37,
                "x": 890,
                "y": 89
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "9d105475-6b13-425c-8814-03c90ee27bb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 85,
                "offset": 2,
                "shift": 39,
                "w": 36,
                "x": 852,
                "y": 89
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "a4d3e15a-c8dc-470e-b452-9825c88d852a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 85,
                "offset": 3,
                "shift": 18,
                "w": 11,
                "x": 839,
                "y": 89
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "372b0144-b26b-4d6a-8d0f-5b9d6256d414",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 85,
                "offset": 3,
                "shift": 43,
                "w": 37,
                "x": 800,
                "y": 89
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "2483bfa7-da83-41fe-8139-151df624aecf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 85,
                "offset": 3,
                "shift": 43,
                "w": 37,
                "x": 761,
                "y": 89
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "7071042f-320e-4c33-a539-121da9413987",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 85,
                "offset": 3,
                "shift": 45,
                "w": 38,
                "x": 721,
                "y": 89
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "fa8ba433-df80-4e04-b2db-6fafb545882f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 85,
                "offset": 3,
                "shift": 43,
                "w": 37,
                "x": 982,
                "y": 89
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "493960ec-8d50-4ecd-8402-2067d30bd749",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 85,
                "offset": 3,
                "shift": 43,
                "w": 37,
                "x": 395,
                "y": 176
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "9b53cef8-d80f-4bf5-b098-d4df9d043564",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 85,
                "offset": 3,
                "shift": 38,
                "w": 34,
                "x": 434,
                "y": 176
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "d98e6859-84a5-4000-80e1-01d8758ae5db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 85,
                "offset": 3,
                "shift": 43,
                "w": 37,
                "x": 470,
                "y": 176
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "b56aba24-8c15-40c6-a0c2-af3eefe37d70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 85,
                "offset": 3,
                "shift": 43,
                "w": 37,
                "x": 206,
                "y": 263
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "f17c3f62-abfa-42d5-ab07-31eb999c077a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 85,
                "offset": 3,
                "shift": 18,
                "w": 11,
                "x": 193,
                "y": 263
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "c6ae5ea2-109a-4c77-9318-d58f91151b28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 85,
                "offset": 3,
                "shift": 18,
                "w": 11,
                "x": 180,
                "y": 263
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "3f273a1c-2321-48fe-8b05-7ead2ca4bedb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 85,
                "offset": 3,
                "shift": 30,
                "w": 23,
                "x": 155,
                "y": 263
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "7cd7bba5-2ca2-44dd-b64a-48a17bbce0eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 85,
                "offset": 3,
                "shift": 40,
                "w": 33,
                "x": 120,
                "y": 263
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "bda4afe0-63ac-40f5-866a-4af8f7049f99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 85,
                "offset": 3,
                "shift": 30,
                "w": 23,
                "x": 95,
                "y": 263
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "53e298a6-1c4b-4504-b8e4-04bec0d80dd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 85,
                "offset": 3,
                "shift": 43,
                "w": 37,
                "x": 56,
                "y": 263
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "2501ad08-0428-4504-bc03-4d4150388c4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 85,
                "offset": 2,
                "shift": 57,
                "w": 52,
                "x": 2,
                "y": 263
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "94ccb85c-3917-4871-9fee-a90593e062c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 85,
                "offset": -2,
                "shift": 42,
                "w": 47,
                "x": 960,
                "y": 176
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "9d572951-09d5-4fff-a4aa-c7807f21bf92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 85,
                "offset": 3,
                "shift": 45,
                "w": 41,
                "x": 917,
                "y": 176
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "f78f5de1-2871-4b10-a83a-65217058996c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 85,
                "offset": 3,
                "shift": 43,
                "w": 37,
                "x": 878,
                "y": 176
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "aa4c607d-254f-4eea-b630-4d54d7cc89dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 85,
                "offset": 3,
                "shift": 43,
                "w": 37,
                "x": 839,
                "y": 176
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "c31253b2-1d80-474a-9ce3-91cc7a85c1a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 85,
                "offset": 3,
                "shift": 39,
                "w": 33,
                "x": 804,
                "y": 176
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "a193e2b0-834f-4f87-b75f-7b2213c809b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 85,
                "offset": 3,
                "shift": 37,
                "w": 33,
                "x": 769,
                "y": 176
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "7b9cca05-bf89-45fb-a25e-2933b8262f05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 85,
                "offset": 3,
                "shift": 43,
                "w": 37,
                "x": 730,
                "y": 176
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "f8bc0b04-124c-45c6-b8e1-d4fe445f7fd1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 85,
                "offset": 3,
                "shift": 43,
                "w": 37,
                "x": 691,
                "y": 176
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "2d90ef27-b609-4538-912a-cabb1f9cbd23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 85,
                "offset": 3,
                "shift": 18,
                "w": 11,
                "x": 678,
                "y": 176
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "1d17b18b-94ed-4a4a-8f82-657a204bab3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 85,
                "offset": 2,
                "shift": 41,
                "w": 36,
                "x": 640,
                "y": 176
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "68e0a19e-f69d-4804-a1ee-4fe8c1cfb520",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 85,
                "offset": 3,
                "shift": 41,
                "w": 37,
                "x": 601,
                "y": 176
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "cac07dd7-501b-4843-b589-9f1eaed76a69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 85,
                "offset": 3,
                "shift": 37,
                "w": 33,
                "x": 566,
                "y": 176
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "427caaaa-9c29-43bf-95d7-0db408e7705c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 85,
                "offset": 3,
                "shift": 62,
                "w": 55,
                "x": 509,
                "y": 176
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "44ad7ab7-408f-4787-92a5-dedc63001c8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 85,
                "offset": 3,
                "shift": 43,
                "w": 37,
                "x": 682,
                "y": 89
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "6b529ab2-0063-499b-81fb-0b7035b2d39a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 85,
                "offset": 2,
                "shift": 39,
                "w": 36,
                "x": 644,
                "y": 89
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "8a950145-0d97-45aa-b787-2f2fd560e228",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 85,
                "offset": 3,
                "shift": 45,
                "w": 38,
                "x": 604,
                "y": 89
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "7c7c5637-c33d-42d1-889e-a135966d3e3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 85,
                "offset": 2,
                "shift": 41,
                "w": 37,
                "x": 837,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "9efde8d2-b353-40b1-923a-a6def6803914",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 85,
                "offset": 3,
                "shift": 44,
                "w": 40,
                "x": 774,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "b92c7393-f2f9-4363-9aaa-09ee0fcb359d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 85,
                "offset": 3,
                "shift": 43,
                "w": 37,
                "x": 735,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "1e0c53ef-f91c-4c05-9e78-6f71dfacff85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 85,
                "offset": 0,
                "shift": 37,
                "w": 36,
                "x": 697,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "193e4d7b-3f8c-4a78-9aa5-2ae7eb01007d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 85,
                "offset": 3,
                "shift": 43,
                "w": 37,
                "x": 658,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "217fe629-fde1-4ee9-aee5-ea435ef680f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 85,
                "offset": -2,
                "shift": 42,
                "w": 47,
                "x": 609,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "e21816a0-9690-479e-8faf-8bce96ea39b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 85,
                "offset": -2,
                "shift": 72,
                "w": 76,
                "x": 531,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "8a869594-1ee5-40b0-be56-a76c0244dc69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 85,
                "offset": 0,
                "shift": 43,
                "w": 44,
                "x": 485,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "54a6959e-7ce6-452f-8542-8a07f5845452",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 85,
                "offset": -2,
                "shift": 39,
                "w": 44,
                "x": 439,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "2d576cf5-2ff0-46e2-9f34-72a53fbf448b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 85,
                "offset": 3,
                "shift": 43,
                "w": 37,
                "x": 400,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "c166aa87-9789-4a1c-9be8-171ed74edfa3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 85,
                "offset": 3,
                "shift": 26,
                "w": 19,
                "x": 816,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "d442e689-e408-4a82-8b69-3b23022cc77d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 85,
                "offset": 0,
                "shift": 36,
                "w": 37,
                "x": 361,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "4a39fd91-8d09-4486-9b35-596167565e60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 85,
                "offset": 3,
                "shift": 26,
                "w": 19,
                "x": 312,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "40de2c14-4d3b-4029-8b31-dacb440308e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 85,
                "offset": 5,
                "shift": 58,
                "w": 48,
                "x": 262,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "a1fde3b1-b1a1-4a01-8f7d-e1fe8beb27ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 85,
                "offset": -2,
                "shift": 40,
                "w": 44,
                "x": 216,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "4d765499-58e4-4e34-9ddc-f0c664f90d6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 85,
                "offset": 19,
                "shift": 58,
                "w": 20,
                "x": 194,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "839affc7-f31d-4951-8a33-fca63c9710bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 85,
                "offset": -2,
                "shift": 33,
                "w": 37,
                "x": 155,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "4a9966dc-ed1f-4b7a-99b9-43c7e52dde89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 85,
                "offset": 3,
                "shift": 37,
                "w": 33,
                "x": 120,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "0ded4a68-1c4d-4a46-bf79-51f105228e66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 85,
                "offset": 3,
                "shift": 36,
                "w": 30,
                "x": 88,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "588f20f2-f358-4353-aed1-c0829250b6e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 85,
                "offset": 3,
                "shift": 36,
                "w": 30,
                "x": 56,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "673116c1-13af-4255-8308-9a2b25ac172d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 85,
                "offset": 3,
                "shift": 33,
                "w": 26,
                "x": 28,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "defcafba-a057-4525-8d1c-2a586444c01e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 85,
                "offset": 3,
                "shift": 31,
                "w": 26,
                "x": 333,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "e393a153-cb2d-404e-9268-8aa11fd1621b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 85,
                "offset": 3,
                "shift": 36,
                "w": 30,
                "x": 876,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "e9686279-36b1-45a2-9624-63e4de90f1f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 85,
                "offset": 3,
                "shift": 36,
                "w": 30,
                "x": 210,
                "y": 89
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "e2d12a33-7e0b-4ffa-9c88-f1a68fc6f897",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 85,
                "offset": 3,
                "shift": 16,
                "w": 9,
                "x": 908,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "3a856e86-2a93-4cbb-9deb-b904058dced0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 85,
                "offset": 2,
                "shift": 34,
                "w": 29,
                "x": 541,
                "y": 89
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "6a771c16-3e00-4221-9c63-f0771e7a15fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 85,
                "offset": 3,
                "shift": 34,
                "w": 30,
                "x": 509,
                "y": 89
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "9bf8280e-7139-4eab-a41f-c7cb4cfd35f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 85,
                "offset": 3,
                "shift": 31,
                "w": 26,
                "x": 481,
                "y": 89
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "887a9595-9210-4098-8905-235fd36e0751",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 85,
                "offset": 3,
                "shift": 51,
                "w": 44,
                "x": 435,
                "y": 89
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "3aff1f69-b8ac-4f87-8959-58b23d185876",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 85,
                "offset": 3,
                "shift": 36,
                "w": 30,
                "x": 403,
                "y": 89
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "13c72d51-f351-4ea9-8c06-88fbc53ac368",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 85,
                "offset": 2,
                "shift": 32,
                "w": 29,
                "x": 372,
                "y": 89
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "aea2670b-2a19-41a0-89ae-b73c19602bc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 85,
                "offset": 3,
                "shift": 35,
                "w": 31,
                "x": 339,
                "y": 89
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "c11678f0-36de-4dfb-af3a-870e65157ce0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 85,
                "offset": 2,
                "shift": 33,
                "w": 30,
                "x": 307,
                "y": 89
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "d9e0c0b0-9c6d-4615-b197-4c19f798e19b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 85,
                "offset": 3,
                "shift": 37,
                "w": 32,
                "x": 273,
                "y": 89
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "624c599e-f644-4906-a3d7-19c7cea8fd0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 85,
                "offset": 3,
                "shift": 36,
                "w": 30,
                "x": 572,
                "y": 89
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "5ecc170f-7d1b-491a-9390-566da0719c1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 85,
                "offset": 0,
                "shift": 30,
                "w": 29,
                "x": 242,
                "y": 89
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "bfecb20a-7515-47b8-9437-2f844a9551d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 85,
                "offset": 3,
                "shift": 36,
                "w": 30,
                "x": 178,
                "y": 89
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "4d9edc56-eccd-4fda-85cc-4e524a8ffe21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 85,
                "offset": -2,
                "shift": 33,
                "w": 37,
                "x": 139,
                "y": 89
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "07dd2fd2-bdcb-4f50-a450-14611b62d609",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 85,
                "offset": -2,
                "shift": 56,
                "w": 61,
                "x": 76,
                "y": 89
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "27700854-06f6-46be-bfa0-48131f3ac505",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 85,
                "offset": 0,
                "shift": 35,
                "w": 35,
                "x": 39,
                "y": 89
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "ae86c55b-db99-45e8-b680-166b29870bbf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 85,
                "offset": -2,
                "shift": 31,
                "w": 35,
                "x": 2,
                "y": 89
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "f048b47e-dba0-41bf-be9c-ed0db8012d82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 85,
                "offset": 3,
                "shift": 36,
                "w": 30,
                "x": 974,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "ca32f8d0-cc4e-43bb-9af4-9751950c2af9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 85,
                "offset": 3,
                "shift": 26,
                "w": 19,
                "x": 953,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "5563a9b9-0658-4645-b227-3617f02d1f0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 85,
                "offset": 3,
                "shift": 18,
                "w": 11,
                "x": 940,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "9ca56f62-3830-49cc-83da-2b42ccff9ab0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 85,
                "offset": 3,
                "shift": 26,
                "w": 19,
                "x": 919,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "d8c0ae37-43ba-44d6-9723-6f957bcd6787",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 85,
                "offset": 7,
                "shift": 58,
                "w": 44,
                "x": 245,
                "y": 263
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "0c923098-1239-4e12-a7e4-ab42c2028d5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 85,
                "offset": 19,
                "shift": 93,
                "w": 55,
                "x": 291,
                "y": 263
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "d18ed9ae-f1e2-4aff-acd2-09e83976191b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 65,
            "second": 84
        },
        {
            "id": "917a56df-c04d-4fd5-aeb3-d81c6b10dba0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 65,
            "second": 116
        },
        {
            "id": "b74f6d15-9a07-4629-904e-91f97afad8b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 84,
            "second": 65
        },
        {
            "id": "2d3b4b14-18c3-4512-96ae-b10bc7842540",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 84,
            "second": 97
        },
        {
            "id": "99948660-4800-457b-8835-9f1d2eeac10b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 97,
            "second": 84
        },
        {
            "id": "58e1b61e-2bf1-4dce-b901-cf9392ba4c07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 97,
            "second": 116
        },
        {
            "id": "90d11b94-0314-4b19-bb7d-7e0bda17fc3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 116,
            "second": 65
        },
        {
            "id": "dc3197b2-df0b-406b-b752-f4329709eb55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 116,
            "second": 97
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 72,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}