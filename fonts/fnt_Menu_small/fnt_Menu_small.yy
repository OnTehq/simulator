{
    "id": "06e7d047-36c2-4364-b537-16cb4140ad95",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_Menu_small",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 12,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "SF Atarian System",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "2ebbb9de-1add-44ad-baa1-c5b8e52e832b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 57,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "104f7648-c9bb-4752-8c9b-137a57253f8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 57,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 129,
                "y": 179
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "00f9c387-8081-4f66-8922-2057c4726577",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 57,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 111,
                "y": 179
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "77fce930-0e41-4c90-85b1-2410b28af983",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 57,
                "offset": 2,
                "shift": 43,
                "w": 38,
                "x": 71,
                "y": 179
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "f358f3c3-54d5-491a-93c4-977afb854d72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 57,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 49,
                "y": 179
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "26a64cf3-3e7a-4c02-8874-b672577c2a6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 57,
                "offset": 2,
                "shift": 49,
                "w": 45,
                "x": 2,
                "y": 179
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "7dbccd53-0250-4b9f-bdfb-8a44754f56a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 57,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 471,
                "y": 120
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "246d4d0c-8864-477e-9eb8-ebea32165479",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 57,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 461,
                "y": 120
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "3fd64ff2-9b56-4e7f-9a66-5d10fa95e59e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 57,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 446,
                "y": 120
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "966ae7b1-befa-4119-8ac1-d66a49a6d2a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 57,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 431,
                "y": 120
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "026594fd-420b-4ca4-bc53-6e24aac9a0ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 57,
                "offset": 2,
                "shift": 55,
                "w": 51,
                "x": 139,
                "y": 179
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "e6fe9ec1-fc50-4552-93dc-d1cd0927926e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 57,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 412,
                "y": 120
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "60df9ec3-7617-4f6c-865c-7bca44abf189",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 57,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 375,
                "y": 120
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "c29ed2ec-6a32-4ff7-b44a-d12711f47855",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 57,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 356,
                "y": 120
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "6210055f-0184-4fda-9a53-2b46ed686366",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 57,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 346,
                "y": 120
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "58959b05-c304-4af6-9411-1f8de603678d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 57,
                "offset": 0,
                "shift": 24,
                "w": 25,
                "x": 319,
                "y": 120
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "e4fcb248-6505-4c2c-bfb7-1ca952b747a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 57,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 293,
                "y": 120
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "fa1cab36-b007-48b0-a67f-b24b59477ad2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 57,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 283,
                "y": 120
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "9d42d782-205b-4a48-b07d-f5297445cb1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 57,
                "offset": 2,
                "shift": 29,
                "w": 25,
                "x": 256,
                "y": 120
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "bafa00d7-d704-46d4-bf1e-8bde01ecdb94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 57,
                "offset": 2,
                "shift": 29,
                "w": 25,
                "x": 229,
                "y": 120
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "0bcd4af4-fa28-4847-8b94-c031d3c044ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 57,
                "offset": 2,
                "shift": 30,
                "w": 26,
                "x": 201,
                "y": 120
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "cf17e7ea-5644-4869-8a28-385d96f7957c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 57,
                "offset": 2,
                "shift": 29,
                "w": 25,
                "x": 385,
                "y": 120
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "1ff69d12-b2f0-49c5-93cc-83beb57857ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 57,
                "offset": 2,
                "shift": 29,
                "w": 25,
                "x": 192,
                "y": 179
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "b7315eb6-08ac-4303-a5e8-a59486e1f343",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 57,
                "offset": 2,
                "shift": 26,
                "w": 23,
                "x": 219,
                "y": 179
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "63a556a0-6e49-4027-b8ad-ebecb2d2a9c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 57,
                "offset": 2,
                "shift": 29,
                "w": 25,
                "x": 244,
                "y": 179
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "09b7fb96-34f4-4ef6-bf9e-3a0b0763fb50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 57,
                "offset": 2,
                "shift": 29,
                "w": 25,
                "x": 263,
                "y": 238
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "38543670-6d6c-46c3-839d-f20589313638",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 57,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 253,
                "y": 238
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "81759d25-2241-45c5-831b-0d5f410e26b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 57,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 243,
                "y": 238
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "7221ec4b-cdd8-4a67-be32-6e33d02540b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 57,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 225,
                "y": 238
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "30cf1a7c-32a7-4a3f-979d-dfe3d36d472e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 57,
                "offset": 2,
                "shift": 27,
                "w": 22,
                "x": 201,
                "y": 238
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "051a0b90-6349-46e9-9376-067440fd4a4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 57,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 183,
                "y": 238
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "d391e4f1-e597-48c2-ae9d-38aa6b46a18d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 57,
                "offset": 2,
                "shift": 29,
                "w": 25,
                "x": 156,
                "y": 238
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "9a389563-4607-4f69-a12d-a220d1f325ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 57,
                "offset": 1,
                "shift": 38,
                "w": 35,
                "x": 119,
                "y": 238
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "8cc5c00d-c290-456e-b7ce-565ceeb1aa9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 57,
                "offset": -2,
                "shift": 28,
                "w": 32,
                "x": 85,
                "y": 238
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "7f3f6746-c61d-4dfe-868f-f7af00700384",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 57,
                "offset": 2,
                "shift": 30,
                "w": 27,
                "x": 56,
                "y": 238
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "a7d96f70-b125-4036-886f-d52ed41454ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 57,
                "offset": 2,
                "shift": 29,
                "w": 25,
                "x": 29,
                "y": 238
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "52d8cb53-3302-4053-bc89-4d17670f1dab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 57,
                "offset": 2,
                "shift": 29,
                "w": 25,
                "x": 2,
                "y": 238
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "c86bcc90-91b2-4aae-b394-7badebcd320d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 57,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 475,
                "y": 179
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "649ed9f5-1f45-4975-8b3a-560fc62e2022",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 57,
                "offset": 2,
                "shift": 25,
                "w": 22,
                "x": 451,
                "y": 179
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "74810cd6-b6e3-4b57-8187-3c1e612de881",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 57,
                "offset": 2,
                "shift": 29,
                "w": 25,
                "x": 424,
                "y": 179
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "8593ee26-8e20-4e41-999b-a7b29d8ac407",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 57,
                "offset": 2,
                "shift": 29,
                "w": 25,
                "x": 397,
                "y": 179
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "28eae8ef-95ea-47ee-863e-35f6a5786dfe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 57,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 387,
                "y": 179
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "ea399485-ea35-4985-b04e-1ac7c61e1716",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 57,
                "offset": 1,
                "shift": 27,
                "w": 24,
                "x": 361,
                "y": 179
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "cf03bb8e-b3ca-40df-9f35-9c04f6cfe027",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 57,
                "offset": 2,
                "shift": 27,
                "w": 25,
                "x": 334,
                "y": 179
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "df9acb33-a62f-43b7-8abb-561a47e5379f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 57,
                "offset": 2,
                "shift": 25,
                "w": 22,
                "x": 310,
                "y": 179
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "e412574a-83e3-45b9-b7ae-a047bcdb33d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 57,
                "offset": 2,
                "shift": 41,
                "w": 37,
                "x": 271,
                "y": 179
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "8c0abfe0-f426-4808-a87e-e2eee909a719",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 57,
                "offset": 2,
                "shift": 29,
                "w": 25,
                "x": 174,
                "y": 120
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "45c02ddc-a966-4ae7-bb5b-ac667e5a65d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 57,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 148,
                "y": 120
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "d3f73e93-7f27-4f51-a560-158bdbf8fdf3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 57,
                "offset": 2,
                "shift": 30,
                "w": 26,
                "x": 120,
                "y": 120
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "5e0b6b82-3833-4013-8e2a-27d1b90af3d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 57,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 73,
                "y": 61
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "f4e3bbf5-ce44-4733-810f-3c7d97ecab37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 57,
                "offset": 2,
                "shift": 30,
                "w": 27,
                "x": 29,
                "y": 61
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "3ea783c6-3faa-4a42-acde-83f2947478e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 57,
                "offset": 2,
                "shift": 29,
                "w": 25,
                "x": 2,
                "y": 61
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "1f4e692b-37d8-43a6-8f11-abd5e1a70f1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 57,
                "offset": 0,
                "shift": 25,
                "w": 24,
                "x": 483,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "a0ac7557-dfff-480d-acc4-df402c9e4a84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 57,
                "offset": 2,
                "shift": 29,
                "w": 25,
                "x": 456,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "5135dd9a-0cdb-4a6d-a5d4-4f324b9517e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 57,
                "offset": -2,
                "shift": 28,
                "w": 32,
                "x": 422,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "48dfbb9c-2838-4dbc-9629-0e44da01f5a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 57,
                "offset": -2,
                "shift": 48,
                "w": 52,
                "x": 368,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "4af74da1-2b6d-4742-8897-9077cfbdb604",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 57,
                "offset": 0,
                "shift": 29,
                "w": 29,
                "x": 337,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "83a941b2-e318-469b-9f2b-af8b7d48d136",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 57,
                "offset": -2,
                "shift": 26,
                "w": 30,
                "x": 305,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "a75545f0-6903-4480-acb8-6d9c7c7d1f5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 57,
                "offset": 2,
                "shift": 29,
                "w": 25,
                "x": 278,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "d814f275-4b4a-435f-bafd-49c72fd5eb39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 57,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 58,
                "y": 61
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "6ef11fd5-14c0-4a39-a12c-6751adef9440",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 57,
                "offset": 0,
                "shift": 24,
                "w": 25,
                "x": 251,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "4031374c-a7d0-4d13-9388-8325e9c821c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 57,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 216,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "7dd3237e-3c5f-4675-9493-741a2d012612",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 57,
                "offset": 3,
                "shift": 38,
                "w": 32,
                "x": 182,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "794ac863-188b-4949-8cb6-b948ce1d53d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 57,
                "offset": -1,
                "shift": 27,
                "w": 29,
                "x": 151,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "ecd54af9-b144-4466-a963-2b4868fef2f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 57,
                "offset": 13,
                "shift": 38,
                "w": 13,
                "x": 136,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "0dccd4fd-4825-4a32-95fd-bc33e3a58c59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 57,
                "offset": -2,
                "shift": 22,
                "w": 26,
                "x": 108,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "cc872a94-9e17-4456-bec8-c97ecbe37b32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 57,
                "offset": 2,
                "shift": 25,
                "w": 22,
                "x": 84,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "dc1200ca-9763-463d-b312-4bc511e56e84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 57,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 62,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "7e798367-8df8-4f81-bcf2-b0bf1e530c87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 57,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 40,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "15d9d558-03dc-446f-a0d7-367a97455c5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 57,
                "offset": 2,
                "shift": 22,
                "w": 18,
                "x": 20,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "55d9f727-c038-413c-a630-fcfddbff52dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 57,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 231,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "5c6a9400-9ebf-4bed-9dd9-ea32e0056f32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 57,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 100,
                "y": 61
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "4794358c-0dc2-4c19-b1c3-36dcdf483801",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 57,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 337,
                "y": 61
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "1ffe18c8-7305-4de9-916b-fdcc2fde09c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 57,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 122,
                "y": 61
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "e16a89db-833c-4766-a3e9-ae819002f547",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 57,
                "offset": 1,
                "shift": 23,
                "w": 20,
                "x": 76,
                "y": 120
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "03517095-357b-4627-ad54-4c3c0487c49c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 57,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 54,
                "y": 120
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "d5aeae3f-8813-46ae-bb46-34d622798ffb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 57,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 34,
                "y": 120
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "38f7b539-71b3-4490-83f3-17271861e083",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 57,
                "offset": 2,
                "shift": 34,
                "w": 30,
                "x": 2,
                "y": 120
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "b83383d7-f5d0-43a0-b71d-6a5a84b447c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 57,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 472,
                "y": 61
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "304f9a65-7d1d-423d-9c53-27d08be011b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 57,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 450,
                "y": 61
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "0c86c077-606d-4195-bb39-2789bf7e1e15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 57,
                "offset": 2,
                "shift": 24,
                "w": 21,
                "x": 427,
                "y": 61
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "06e4dc05-777a-4748-af23-b2d73ea0b7e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 57,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 405,
                "y": 61
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "ae327644-74db-41cb-b0e3-d81ea68ddb84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 57,
                "offset": 2,
                "shift": 24,
                "w": 22,
                "x": 381,
                "y": 61
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "56d53f6f-5373-4d2c-8124-546f57040137",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 57,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 98,
                "y": 120
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "e28cc29c-0228-4c65-ae84-bb4932366ca1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 57,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 359,
                "y": 61
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "3eb5f55d-d3f8-4f5e-89a1-84660b4d10cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 57,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 315,
                "y": 61
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "2642aa80-73f8-4a52-a77b-814dced33ad9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 57,
                "offset": -2,
                "shift": 22,
                "w": 26,
                "x": 287,
                "y": 61
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "fe03ff00-8ecc-40f2-aeac-d95936f71d0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 57,
                "offset": -2,
                "shift": 38,
                "w": 41,
                "x": 244,
                "y": 61
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "e0e65e9d-316e-4082-b9ce-df8831c62199",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 57,
                "offset": 0,
                "shift": 23,
                "w": 24,
                "x": 218,
                "y": 61
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "9b82b9fb-8685-4612-899f-e970d788dc65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 57,
                "offset": -2,
                "shift": 20,
                "w": 24,
                "x": 192,
                "y": 61
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "841898f2-3e80-44f3-b31c-0dea6f7d77fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 57,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 170,
                "y": 61
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "407c3896-87ae-4d28-98da-d36daca177d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 57,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 155,
                "y": 61
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "9db258bc-aa1a-42b1-a6f3-303413c9c806",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 57,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 145,
                "y": 61
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "ffc2f9f3-27f9-4764-9028-512e22f3bcee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 57,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 130,
                "y": 61
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "0b927bd3-cc46-4f01-8549-5b4721a77dcb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 57,
                "offset": 4,
                "shift": 38,
                "w": 30,
                "x": 290,
                "y": 238
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "dbfc32cc-b414-43b7-94eb-6c0998a1086a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 57,
                "offset": 12,
                "shift": 62,
                "w": 38,
                "x": 322,
                "y": 238
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "ab7f0757-fbab-4eac-9475-53a4637e2dac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 65,
            "second": 84
        },
        {
            "id": "2da58dec-3875-4b39-ae60-ba3697e86159",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 116
        },
        {
            "id": "96b0fb99-7c66-41cb-b93d-12ac4b75b7f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 65
        },
        {
            "id": "3ebc2090-a617-4117-835c-ba22171bed7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 97
        },
        {
            "id": "ef30d519-40fc-42b2-8805-ef46354ba2da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 97,
            "second": 84
        },
        {
            "id": "221c48e7-fe7e-4dca-8594-5da009a81a12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 97,
            "second": 116
        },
        {
            "id": "957a3451-df81-4fb9-9a2d-12dea1234c62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 116,
            "second": 65
        },
        {
            "id": "4ca5ed68-f805-464f-a36f-227390df7984",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 116,
            "second": 97
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 48,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}