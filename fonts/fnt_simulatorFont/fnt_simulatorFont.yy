{
    "id": "0dccd83d-2f5d-4a96-841f-4b92518af66a",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_simulatorFont",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "1bf4280b-b8c4-4efc-9101-c33a997ab012",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 37,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "4d361fce-178c-4cfa-9679-37bc5a58d4e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 37,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 118,
                "y": 80
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "8864a46f-91e9-45f6-bc85-536bf261c33b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 37,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 107,
                "y": 80
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "a63ee265-d62c-423e-bbe9-3afed49a5250",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 87,
                "y": 80
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "f52521fd-dfdb-4278-ba15-fe2fd608e03e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 69,
                "y": 80
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "815fa6e9-b946-438c-90f1-7d5ffeb9e979",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 37,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 41,
                "y": 80
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "82b39c55-b44a-4033-b0cf-536ce10a3d3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 37,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 19,
                "y": 80
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "a8dfa1cd-8140-4fad-9d37-6b85a5d43026",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 37,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 13,
                "y": 80
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "d9ccb925-28e6-48d5-a69b-b3d2cc2f965a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 37,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 2,
                "y": 80
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "99065e8c-f8a2-4d16-88ef-7f4cac1eaed9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 37,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 499,
                "y": 41
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "1cd5daa4-0804-4faa-80d4-2628f1ebba21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 37,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 125,
                "y": 80
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "0acbd22d-776d-41b3-82bb-ef8a368b8e93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 37,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 481,
                "y": 41
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "9241d899-dad7-4cc6-84ce-b897fe8080d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 37,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 456,
                "y": 41
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "2e2d91d8-fa32-4820-87a2-2083694e9dee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 37,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 445,
                "y": 41
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "b7a58023-6c1d-4009-afcd-322b2961d945",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 37,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 438,
                "y": 41
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "d5fbf32b-fb60-4b0d-be20-514e77395c98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 37,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 427,
                "y": 41
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "8ad9b2d7-2ccf-48e1-bb24-1c37f51b3428",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 409,
                "y": 41
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "5aef5678-e0fd-4351-80f9-7c64d66a6bc5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 37,
                "offset": 3,
                "shift": 18,
                "w": 9,
                "x": 398,
                "y": 41
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "931d7161-8907-4cee-9fa3-68c223210fe8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 379,
                "y": 41
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "9c633ae7-456d-4f00-836b-ddb52a1dbbff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 361,
                "y": 41
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "845d7cbe-f986-4921-8aef-05f9bb91f73b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 342,
                "y": 41
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "3a26862a-7107-4370-a353-1c793ba65c03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 463,
                "y": 41
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "bb94f563-fa54-4732-b221-c756c0838da3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 138,
                "y": 80
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "bb9954f3-f074-4802-a69c-2bc05110bfaa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 156,
                "y": 80
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "9d205909-658f-47ac-9ebf-b176f59375c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 174,
                "y": 80
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "382bd0c5-c79b-4464-812b-5d94052b9a0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 70,
                "y": 119
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "55d0e248-ec74-43c9-b0d3-29dc88ef5331",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 37,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 63,
                "y": 119
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "102de44c-26cc-4f12-8623-a39f7259a059",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 37,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 56,
                "y": 119
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "07ee783f-02f2-410a-9ddc-95dd10076338",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 37,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 38,
                "y": 119
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "80d3a9f1-677e-48ef-8920-5b4c138c6c89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 37,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 20,
                "y": 119
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "e952a71c-04ee-4dc6-8645-b3d006db8de4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 37,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 2,
                "y": 119
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "35ba59a0-19d7-4582-98b0-1ad6f1580157",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 486,
                "y": 80
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "6cf4ce1f-204c-4978-b58a-31b2a8ea8b31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 37,
                "offset": 1,
                "shift": 32,
                "w": 31,
                "x": 453,
                "y": 80
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "16e92f16-dc87-4ab9-b5cd-881198813149",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 37,
                "offset": -1,
                "shift": 21,
                "w": 23,
                "x": 428,
                "y": 80
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "8b9bd142-c182-4d6c-bbf7-67ef46c31124",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 37,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 408,
                "y": 80
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "6f0f7b86-97f9-4552-8cf4-a28790a3a398",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 37,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 385,
                "y": 80
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "dfbdb943-c2e3-4217-9d68-7b540a2e31d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 37,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 363,
                "y": 80
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "bb7d461e-ea52-491d-b559-9fc561306c5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 37,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 343,
                "y": 80
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "b2b71936-04f3-45c0-9102-766d0d506333",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 37,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 324,
                "y": 80
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "9a89846d-d41b-4a1f-b1cc-c118b4e72b9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 37,
                "offset": 1,
                "shift": 25,
                "w": 22,
                "x": 300,
                "y": 80
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "d9d2b857-6504-476a-becf-90a49245603d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 37,
                "offset": 2,
                "shift": 23,
                "w": 19,
                "x": 279,
                "y": 80
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "06d1b959-a54c-4065-a7c8-91560e16a091",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 37,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 272,
                "y": 80
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "69f0f358-cd64-47b5-b5a2-8e39360be209",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 37,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 256,
                "y": 80
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "db0337b2-b65a-4d86-a040-5fcc66811d47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 37,
                "offset": 2,
                "shift": 21,
                "w": 20,
                "x": 234,
                "y": 80
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "43d47fe6-d754-4aca-b8b7-34b36921528b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 217,
                "y": 80
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "1f791d2d-d183-4906-b395-2149251abb26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 37,
                "offset": 2,
                "shift": 27,
                "w": 23,
                "x": 192,
                "y": 80
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "ca8dddf2-b55d-4e03-a589-1359e1e1fe20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 37,
                "offset": 2,
                "shift": 23,
                "w": 19,
                "x": 321,
                "y": 41
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "1f50e950-387b-48fe-a369-f8ed02dd958b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 37,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 296,
                "y": 41
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "e3e73466-5d9c-4258-8988-e8ffabbbd97c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 37,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 276,
                "y": 41
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "c3c8bf61-bef8-475a-a604-08386d648661",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 37,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 400,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "81399e7f-d200-417a-9db9-6558c5f07e6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 37,
                "offset": 2,
                "shift": 23,
                "w": 21,
                "x": 368,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "36b2df42-cd5a-4047-80fe-1f73b56779d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 37,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 347,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "843d0413-88f8-48be-b373-d490b55e2579",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 37,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 326,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "48332ce6-c5e2-466d-ac34-590352be5bc5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 37,
                "offset": 2,
                "shift": 23,
                "w": 19,
                "x": 305,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "4703fe06-e2c7-44de-acca-8cece97748c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 37,
                "offset": 0,
                "shift": 21,
                "w": 22,
                "x": 281,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "293bfa2f-332b-4902-92dd-51e1fda3f91f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 37,
                "offset": 0,
                "shift": 30,
                "w": 30,
                "x": 249,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "47cd0eb4-fbf2-422d-8fc7-7d74fb24ddd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 37,
                "offset": 0,
                "shift": 21,
                "w": 22,
                "x": 225,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "095afd3c-5324-4ff2-902d-6703c7c164c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 37,
                "offset": 0,
                "shift": 21,
                "w": 22,
                "x": 201,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "4a4073f9-edef-4203-9e45-03d754c1a337",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 37,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 180,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "4fd3a838-3a58-44a6-ac19-a2b6d78cd588",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 37,
                "offset": 2,
                "shift": 9,
                "w": 7,
                "x": 391,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "6cfe7fd0-7f10-44d4-8913-d5dc89c84cf0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 37,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 169,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "32db4926-5576-4177-81dc-76b31c33b977",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 37,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 148,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "5e479c12-f9d0-4a02-97a9-842cdc85087b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 37,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 131,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "caa12c2c-a168-4d9b-b2ca-4c24e6e07ad7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 37,
                "offset": -1,
                "shift": 18,
                "w": 20,
                "x": 109,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "dcdbc5ec-b8fd-4859-ba40-5ce7e3d67b24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 37,
                "offset": 1,
                "shift": 11,
                "w": 7,
                "x": 100,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "7b91c7cf-6e7f-4977-b026-4cad99b473f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 82,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "8bb9cfa3-c52b-4cfc-af47-8d82817807fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 65,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "6434bd03-2d77-4c39-9e5a-6374e39a6b13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 48,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "cbf50f44-349a-486e-b5d0-2613e8d8980f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 31,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "6c96294b-c504-48f1-8479-f1cfbc5bbc31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 13,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "fe3a7d31-1fde-4960-bbcd-612105f0b329",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 37,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 157,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "b90a2fb8-8c3c-46c8-a375-fe3ba1391e88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 425,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "5d78faac-d261-4147-8a38-c37b522a625c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 97,
                "y": 41
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "7806ef08-e1a5-46c3-aa5d-e39befdcf01b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 37,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 442,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "42f775fa-8f94-4c21-b83d-5b8f4cc452ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 37,
                "offset": -2,
                "shift": 7,
                "w": 7,
                "x": 250,
                "y": 41
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "ec495237-01d1-466a-b1c8-a2b2f9fa708b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 37,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 234,
                "y": 41
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "103b1237-4031-4ce6-864d-0c60096b0100",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 37,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 229,
                "y": 41
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "ae873b96-2e0a-4ab7-9b9a-f62c0957ad4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 37,
                "offset": 2,
                "shift": 27,
                "w": 23,
                "x": 204,
                "y": 41
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "5706d8c5-7b72-44cc-a1c0-ca05ca1ffcd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 188,
                "y": 41
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "aaf7d3d8-bba0-4821-b8f0-3443dbee2099",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 170,
                "y": 41
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "15d34a3e-87b3-47c2-a70a-7f6654711be6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 153,
                "y": 41
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "f32fc760-f2af-47de-8229-46743e8f8335",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 136,
                "y": 41
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "0ddf23ba-c848-4555-951c-3fc3dd214f6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 37,
                "offset": 2,
                "shift": 11,
                "w": 10,
                "x": 124,
                "y": 41
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "da05746b-2dd2-48f0-b0c1-1946ebb65aba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 37,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 259,
                "y": 41
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "10413305-1341-4f2c-a569-16c29c0e1660",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 37,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 113,
                "y": 41
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "0d72fbb8-7c52-481d-b4fb-9f052c60ea1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 81,
                "y": 41
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "b01fe4b5-b7f3-4147-b202-b9e335f618c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 37,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 63,
                "y": 41
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "2e02c21b-e754-4a32-af7e-d8069966722c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 37,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 38,
                "y": 41
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "456e529e-a864-4efb-8ae6-bbcef38e84b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 37,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 20,
                "y": 41
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "c7b26e4c-7eb3-4e58-a605-30df2fe66915",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 37,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 2,
                "y": 41
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "db74c81c-da8c-4366-a3e3-f8dc2e91041f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 37,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 477,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "d2f3ec1e-173b-42a0-997b-119b865e127d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 37,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 465,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "fddf4c6a-93ea-45c2-8461-c03cc830d26e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 37,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 459,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "a11083cf-3e34-4017-ab1a-d02693649100",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 37,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 447,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "988ff35f-83a2-4341-965f-12a40e19b6f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 37,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 88,
                "y": 119
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "feef0fdd-8a47-4d7e-afe0-a7824480c48d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 37,
                "offset": 6,
                "shift": 31,
                "w": 19,
                "x": 107,
                "y": 119
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "7c993367-aa6b-450a-b0a2-6c0551578261",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 65
        },
        {
            "id": "4b6b0c52-33c5-431d-b10a-5e1cade4cbed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 84
        },
        {
            "id": "81e889af-3cae-4f67-87f0-5e620aed45aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 89
        },
        {
            "id": "91247004-ba56-4e55-901b-26189b9f281b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 902
        },
        {
            "id": "1dc3d744-cb7b-4395-a6ef-7fb44fe79974",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 913
        },
        {
            "id": "75ae6c29-b4b1-4c94-a6b1-e985b7d4018a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 916
        },
        {
            "id": "2adabfa1-2f4f-41a6-b3c0-49312f4bd724",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 923
        },
        {
            "id": "e4ecfe94-0804-4803-a7fd-550918bd27cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 932
        },
        {
            "id": "08adb127-6c66-4b73-98be-91c6ffd823df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 933
        },
        {
            "id": "d69fcf76-6228-4f8d-8768-7c12d473cd54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 939
        },
        {
            "id": "b3174543-3f51-46bb-8e3b-c3cac4bd2573",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 49,
            "second": 49
        },
        {
            "id": "34283c98-2f90-40cc-b06c-298b219aa76e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 32
        },
        {
            "id": "f20f4386-2727-4cde-ba88-f5f7d283def0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 84
        },
        {
            "id": "4d8826b4-d141-4895-9e95-6648c71a2a65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 86
        },
        {
            "id": "9f1b72b6-3fd7-4d54-a9ad-89c789a2f93d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "758894f1-9d0e-42ae-a3b4-b8eb5c12a150",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 89
        },
        {
            "id": "34193a02-4695-4893-88de-d65b5b44cbba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 118
        },
        {
            "id": "f1036219-1c2d-4667-af2f-95a7550a40a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 119
        },
        {
            "id": "c4bbc06b-9581-42d8-8d54-c4f5196ef702",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 121
        },
        {
            "id": "ea8b3057-4f40-4982-8d43-e56ff9734a5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 160
        },
        {
            "id": "837f61fa-d511-4403-b92a-3cf1d4bec77c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 8217
        },
        {
            "id": "da7263b1-cc81-4955-b148-827376636d97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 44
        },
        {
            "id": "a7f4c7c5-d84f-44fc-8e75-39def541b354",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 46
        },
        {
            "id": "a408429a-88d0-4988-a67b-f3ae9bf1b9f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 65
        },
        {
            "id": "72b6e6b8-863b-40fa-adbf-afb068b2bd5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 32
        },
        {
            "id": "188f880a-9f14-435f-a6ab-d13d9b2ac5e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 84
        },
        {
            "id": "2104d32a-8417-44c8-b3fb-0c55a74009b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 86
        },
        {
            "id": "f1408974-474a-493f-8ddb-56823dd89451",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 87
        },
        {
            "id": "58efb399-7063-4c29-aa66-a6209435e01b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 89
        },
        {
            "id": "1115d13e-9e74-4dab-8968-d3f0377c8f94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "6d86429a-6aef-4770-9e06-1ab2f61a8dbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 160
        },
        {
            "id": "06ac1c0f-d01f-42aa-9cff-8596b9bbf552",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 8217
        },
        {
            "id": "5cf204b6-2c02-4c86-a912-7bee72b1208a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 32
        },
        {
            "id": "b0d528af-edf8-4baf-9c75-9b49434e75b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 44
        },
        {
            "id": "f0c16c6b-7ba9-4fe8-a5de-c69a02afd070",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 46
        },
        {
            "id": "fd1ae545-3ab3-4df3-82d2-003d907b9978",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 65
        },
        {
            "id": "99ebf740-e516-4fd7-a033-a756511d63e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 160
        },
        {
            "id": "9b7970e4-f108-40ae-95da-b6cf5914b382",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 84
        },
        {
            "id": "6c0e2800-1930-45b0-a860-a242bedc87e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 86
        },
        {
            "id": "326d6f86-a94c-4db2-9cc9-0b3bb5cc005a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 87
        },
        {
            "id": "88445939-96c0-4069-aede-0309fb3c10e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 89
        },
        {
            "id": "3b3a3dcf-dc72-44b3-82d7-9b69582b5f01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 32
        },
        {
            "id": "bd029c1e-aee6-49e1-8335-c76dc685683b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 44
        },
        {
            "id": "e092550b-5f60-4bc2-b44e-19ad1e0a64ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 45
        },
        {
            "id": "76cca53d-d956-47a1-8e44-197955242600",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 46
        },
        {
            "id": "b6f7f3cf-2f09-4761-a3d7-48cd1ab16cc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 58
        },
        {
            "id": "880fafbf-616b-4469-b51d-abaf4a465dbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 59
        },
        {
            "id": "fb12538e-08e9-465d-b1c8-0f38e35e8dbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 65
        },
        {
            "id": "1021db3c-896a-4f12-b7ef-0be24ccfa691",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 79
        },
        {
            "id": "be9817f1-5f17-416e-889f-001441314ef5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 97
        },
        {
            "id": "c3363dc3-553b-48b9-8556-0b1544684be2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 99
        },
        {
            "id": "74f13faf-2808-4d74-92e6-509ab8c260bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 101
        },
        {
            "id": "299c4c2a-6b17-46c9-a657-3fe6110ae5c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 105
        },
        {
            "id": "509fb916-cb60-4d2e-97ac-e3e08ecd8dfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 111
        },
        {
            "id": "6350b47f-355c-4347-933f-bdb011d50935",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "844246e6-d8bb-4001-a360-7849f7ee09b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 115
        },
        {
            "id": "31d6ba35-ba96-49f0-95fd-ad3944efda24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "37de6aa0-1766-4ec7-8aa5-bff20df502a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 119
        },
        {
            "id": "35fcf775-8be5-48a1-8bc0-31b93e6b36a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 121
        },
        {
            "id": "af5e8046-935c-4e3b-bcd2-0cd1cc39bd1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 160
        },
        {
            "id": "cfb7aa0d-cb95-464a-9c42-8d1cb7ef3c90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 173
        },
        {
            "id": "4a3daaa2-ff45-4373-adfa-60f3e7b45cfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 894
        },
        {
            "id": "bdf885b2-41b5-4898-819e-2ddb97e4f426",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 44
        },
        {
            "id": "8c14925c-6332-463f-8b89-8ae75a46ea75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 45
        },
        {
            "id": "79e3765f-9af5-4802-9fc7-7596ebb3e704",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 46
        },
        {
            "id": "6db77a93-40e8-4f6e-b945-c9d37ad4cb36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "3ec4cc33-a824-4b28-82e7-d3b89f5b0de2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "bef0a163-482c-414a-9c76-a82c01e7e094",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 65
        },
        {
            "id": "456344cc-6944-443e-bf83-12458d8e47e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 97
        },
        {
            "id": "b5fa8328-de3b-4ed9-8236-62359741e55f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 101
        },
        {
            "id": "e2f074ef-1328-4d12-911a-9c0b6d2498b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 105
        },
        {
            "id": "cadd61ab-b585-43be-8512-b49ea94f35c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 111
        },
        {
            "id": "0aab9fcd-61e2-4b1e-87c9-babceae64a53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "a5c0ca85-dc8d-43e5-80df-97fa73a4ae6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "7537becd-1664-45c9-aa24-b8f3da838d43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 121
        },
        {
            "id": "9af3d4d6-f070-4137-a427-f8147417c976",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 173
        },
        {
            "id": "3e8949d9-87d6-4bdf-8cfe-46381b199266",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 894
        },
        {
            "id": "51117e74-de03-4de2-81ec-12245dd81a87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 44
        },
        {
            "id": "19d3fde8-fe10-475b-b803-06b1b2a617a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 45
        },
        {
            "id": "53789de2-cd48-4d7a-be33-cbb6067bef03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 46
        },
        {
            "id": "c4a20f18-8563-420b-b958-7f64fedbd583",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 58
        },
        {
            "id": "6a66cfb1-62ce-4a68-a0f4-df8ce6468bcf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 59
        },
        {
            "id": "5bc3ce9e-860b-4eba-9e35-d6956c670aec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "8f75d205-0c7b-4159-9da0-10f296202c13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "a7dea511-98e2-4a6f-9559-e026dd46a4fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 101
        },
        {
            "id": "edf46541-2657-4c07-9ffc-bbfd59d4285e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 111
        },
        {
            "id": "449f1c32-595a-4591-8965-4409516166d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 114
        },
        {
            "id": "096a1ade-ce56-4c39-a678-ffbdd877d881",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 117
        },
        {
            "id": "4d192898-e0aa-4e72-b7ea-b9f35030294c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 173
        },
        {
            "id": "6bed2b72-86aa-4aab-8ece-146e06987843",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 894
        },
        {
            "id": "6ef3eb82-3bcb-4b76-b09b-72f79bec5e65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 32
        },
        {
            "id": "85a39419-3f1a-4e37-bcd5-9a823736acd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 44
        },
        {
            "id": "ba33262b-01ab-4073-b04d-594d8acc3be9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 45
        },
        {
            "id": "1af199f4-2ab0-4cd3-a023-57321621af1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 46
        },
        {
            "id": "874f727a-b182-4434-8486-26b011476f0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 58
        },
        {
            "id": "dbb03358-dad1-4af1-b19d-7cc76c39602d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 59
        },
        {
            "id": "29c61056-c9fd-450b-9dd8-bfc36a4cc767",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 65
        },
        {
            "id": "d3debad5-41ce-4cf5-89d8-153a9a0ee0bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 97
        },
        {
            "id": "db73cbfd-f1f1-4363-9ffb-32a771fc3904",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 101
        },
        {
            "id": "6a51070c-0428-4ed1-a671-72fe978deb79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 105
        },
        {
            "id": "18457923-db64-4b98-b5e8-50d6f50238b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 111
        },
        {
            "id": "7286edd5-d718-4ac7-b47a-558acea63a2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 112
        },
        {
            "id": "0e7c65bd-cd7e-4ca0-aec5-a561e349d48c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 113
        },
        {
            "id": "c8a763d0-339f-46dc-b823-6acb39cbb227",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 117
        },
        {
            "id": "825a1c54-0657-46bb-9db5-4d08b5938684",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 118
        },
        {
            "id": "a65cbe6e-ea86-4ea8-95ea-45e0e4abcf43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 160
        },
        {
            "id": "316047c3-d6db-4eda-ac1a-1acb815ddf9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 173
        },
        {
            "id": "5e2a07f9-5a51-45c7-b116-1eeb39b20433",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 894
        },
        {
            "id": "f1cdc96a-2c56-44a1-867a-94528cae117a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 102
        },
        {
            "id": "6a91b0fa-ff4c-4f85-a19b-9edb71a5b1bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8217
        },
        {
            "id": "3d650e03-0cf8-49bc-a609-872338cfd54b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 44
        },
        {
            "id": "6d08a5d7-63af-43ce-b3a4-2429d74a4cb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 46
        },
        {
            "id": "45b69e5f-3ca9-4c30-9f59-d164e3f848ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 8217
        },
        {
            "id": "9736fba0-137e-4be2-b4c8-a959d7599f54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 44
        },
        {
            "id": "7c878370-b18c-4081-9c4f-99f0acf79b13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 46
        },
        {
            "id": "e6eb28cf-e23a-4b0f-a371-89ab66fa4b36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 44
        },
        {
            "id": "1048479b-ebbc-4462-9cb1-91fd320bfbc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 46
        },
        {
            "id": "0ba728cc-2fab-4bc1-a128-0a501d04d22e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 44
        },
        {
            "id": "c490ef2d-43a6-4f14-a2b9-ac40ad514e6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 24,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}