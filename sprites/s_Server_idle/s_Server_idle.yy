{
    "id": "d5286849-c600-4c79-a24b-0c9c730ac728",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_Server_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7f9c4af4-bed1-498c-968c-0749159e89be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5286849-c600-4c79-a24b-0c9c730ac728",
            "compositeImage": {
                "id": "bc9c0c1f-35aa-49e7-8ec0-0aa586d2e2da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f9c4af4-bed1-498c-968c-0749159e89be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddccdb16-5b0c-4234-b64b-45caea5ae42b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f9c4af4-bed1-498c-968c-0749159e89be",
                    "LayerId": "36d04fba-704e-4a45-82e1-5265796465ef"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "36d04fba-704e-4a45-82e1-5265796465ef",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d5286849-c600-4c79-a24b-0c9c730ac728",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}