{
    "id": "b2eef806-5ba7-45ab-bd5c-6d6aada82bb4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_Machine_menu",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 8,
    "bbox_right": 26,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cfee01ff-28e4-477f-8c1d-6b612fb354ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b2eef806-5ba7-45ab-bd5c-6d6aada82bb4",
            "compositeImage": {
                "id": "56e21a69-05dd-4c1a-8e2b-fdf50832f04d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cfee01ff-28e4-477f-8c1d-6b612fb354ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acd5421b-7986-48b7-afef-5efbc456e911",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cfee01ff-28e4-477f-8c1d-6b612fb354ac",
                    "LayerId": "c481a34d-fa46-4b81-8de3-8d67bc63949e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 36,
    "layers": [
        {
            "id": "c481a34d-fa46-4b81-8de3-8d67bc63949e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b2eef806-5ba7-45ab-bd5c-6d6aada82bb4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 36,
    "xorig": 18,
    "yorig": 18
}