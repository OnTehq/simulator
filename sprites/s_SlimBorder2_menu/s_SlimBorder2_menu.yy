{
    "id": "4f0d0e76-e242-4030-9b1d-a4fe8b5991de",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_SlimBorder2_menu",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 0,
    "bbox_right": 99,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "99b291e5-be4d-4824-a734-a80c51904136",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f0d0e76-e242-4030-9b1d-a4fe8b5991de",
            "compositeImage": {
                "id": "b669e6e7-42db-4583-8465-25c183a6a5f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99b291e5-be4d-4824-a734-a80c51904136",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb2e62ce-b497-4bf5-9ada-45d2f8ae419d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99b291e5-be4d-4824-a734-a80c51904136",
                    "LayerId": "d96f06dc-8f65-4aae-af23-3dc9f7dd0bc9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "d96f06dc-8f65-4aae-af23-3dc9f7dd0bc9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4f0d0e76-e242-4030-9b1d-a4fe8b5991de",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": -219,
    "yorig": 44
}