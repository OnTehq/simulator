{
    "id": "6b4ca49b-7d51-4b7c-9019-ed99e4997c42",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_Client_waiting_menu",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 45,
    "bbox_left": 0,
    "bbox_right": 47,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9f60a9b2-7dc6-491c-b56f-6e5f5f24da96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b4ca49b-7d51-4b7c-9019-ed99e4997c42",
            "compositeImage": {
                "id": "2f65b0a6-ab74-4662-bec5-028836e1ac66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f60a9b2-7dc6-491c-b56f-6e5f5f24da96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2bf74bb0-9f96-4ec5-9d9c-55daa7418aeb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f60a9b2-7dc6-491c-b56f-6e5f5f24da96",
                    "LayerId": "59e30802-1fed-440f-9e6d-2ce2bb3c41ac"
                }
            ]
        },
        {
            "id": "d2e8bee3-c1d0-4f56-83c7-05b04c63e7e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b4ca49b-7d51-4b7c-9019-ed99e4997c42",
            "compositeImage": {
                "id": "2d6b5798-1a45-431e-b072-4c48e1250370",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2e8bee3-c1d0-4f56-83c7-05b04c63e7e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc66430b-8ca0-46b6-868f-775c0b47fd75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2e8bee3-c1d0-4f56-83c7-05b04c63e7e0",
                    "LayerId": "59e30802-1fed-440f-9e6d-2ce2bb3c41ac"
                }
            ]
        },
        {
            "id": "4fcdbaec-f409-4e58-938b-7e4686799809",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b4ca49b-7d51-4b7c-9019-ed99e4997c42",
            "compositeImage": {
                "id": "cb379996-a798-4e8f-a9a4-0d0b45688ca2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fcdbaec-f409-4e58-938b-7e4686799809",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76d368b4-2901-4678-925b-ed2265ad5d03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fcdbaec-f409-4e58-938b-7e4686799809",
                    "LayerId": "59e30802-1fed-440f-9e6d-2ce2bb3c41ac"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "59e30802-1fed-440f-9e6d-2ce2bb3c41ac",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6b4ca49b-7d51-4b7c-9019-ed99e4997c42",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}