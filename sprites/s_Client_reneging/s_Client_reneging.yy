{
    "id": "4ae9abb0-deec-453c-9a03-564b2f0174fe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_Client_reneging",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b1a84f0d-6f29-49ea-a158-dbdc489430f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ae9abb0-deec-453c-9a03-564b2f0174fe",
            "compositeImage": {
                "id": "f8f5d2e3-7dee-4ac0-b398-466a275d99f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1a84f0d-6f29-49ea-a158-dbdc489430f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be9a1427-a1dc-4300-8f83-52496d38ac51",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1a84f0d-6f29-49ea-a158-dbdc489430f9",
                    "LayerId": "b25dd346-dffb-457a-bd93-a1cdb7d2dea3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b25dd346-dffb-457a-bd93-a1cdb7d2dea3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4ae9abb0-deec-453c-9a03-564b2f0174fe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}