{
    "id": "441cd74b-12ce-454e-bfb9-cd68269c7ea7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_Machine",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 8,
    "bbox_right": 26,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "885f53a5-075e-4cbf-b873-c87abad0d633",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "441cd74b-12ce-454e-bfb9-cd68269c7ea7",
            "compositeImage": {
                "id": "cddaa87d-e26a-40dc-85c9-d5f21433f23c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "885f53a5-075e-4cbf-b873-c87abad0d633",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f282e3a5-5430-4121-9683-ade528001f23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "885f53a5-075e-4cbf-b873-c87abad0d633",
                    "LayerId": "1dfb3876-c394-41d7-aed8-2e08233d3572"
                }
            ]
        },
        {
            "id": "e93efe4d-d281-46f3-b6fe-1fdab104399f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "441cd74b-12ce-454e-bfb9-cd68269c7ea7",
            "compositeImage": {
                "id": "17dd9be9-a62b-43e2-b5db-b6e3049639e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e93efe4d-d281-46f3-b6fe-1fdab104399f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72a14e02-0e3c-486f-ac78-9f13c92be03e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e93efe4d-d281-46f3-b6fe-1fdab104399f",
                    "LayerId": "1dfb3876-c394-41d7-aed8-2e08233d3572"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 36,
    "layers": [
        {
            "id": "1dfb3876-c394-41d7-aed8-2e08233d3572",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "441cd74b-12ce-454e-bfb9-cd68269c7ea7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 36,
    "xorig": 18,
    "yorig": 18
}