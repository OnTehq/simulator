{
    "id": "a9b6baf7-fd61-45e4-9309-78581cebc1a1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_textBorder",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 0,
    "bbox_right": 199,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a4c98b98-96f7-4b61-9488-5a54662fda7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9b6baf7-fd61-45e4-9309-78581cebc1a1",
            "compositeImage": {
                "id": "c7997f83-5271-49b2-9ad6-22ce5f496871",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4c98b98-96f7-4b61-9488-5a54662fda7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b43333e1-450b-44f1-beb1-9ac93e2d9429",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4c98b98-96f7-4b61-9488-5a54662fda7c",
                    "LayerId": "c2180e26-9340-42f1-9b4b-8dd5caa4d5e4"
                }
            ]
        },
        {
            "id": "930c94d8-3a3c-4c27-9ab2-c61b9f19b68c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9b6baf7-fd61-45e4-9309-78581cebc1a1",
            "compositeImage": {
                "id": "0690e902-0cd4-4ce2-ab37-22c97fc472ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "930c94d8-3a3c-4c27-9ab2-c61b9f19b68c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3735d5dc-8af5-4d67-b98c-2f43dd5d9aa5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "930c94d8-3a3c-4c27-9ab2-c61b9f19b68c",
                    "LayerId": "c2180e26-9340-42f1-9b4b-8dd5caa4d5e4"
                }
            ]
        },
        {
            "id": "40a3f937-0180-46f8-a5a4-85c72151e38b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9b6baf7-fd61-45e4-9309-78581cebc1a1",
            "compositeImage": {
                "id": "cd9baac5-6e30-4a44-880a-e67e94f32214",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40a3f937-0180-46f8-a5a4-85c72151e38b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32f6c606-547d-49ca-8d92-b132268c7baa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40a3f937-0180-46f8-a5a4-85c72151e38b",
                    "LayerId": "c2180e26-9340-42f1-9b4b-8dd5caa4d5e4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "c2180e26-9340-42f1-9b4b-8dd5caa4d5e4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a9b6baf7-fd61-45e4-9309-78581cebc1a1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 100,
    "yorig": 40
}