{
    "id": "2ff1639a-c32a-4f19-a2e5-c1dc36c286d0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_Client_served",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3fdf351e-5941-4c9f-ac0b-ed19db7c1bde",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ff1639a-c32a-4f19-a2e5-c1dc36c286d0",
            "compositeImage": {
                "id": "9c3edea4-7546-4569-a225-d6bc3c77d292",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fdf351e-5941-4c9f-ac0b-ed19db7c1bde",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0aabeb27-86ad-4c44-a46f-ede3f8a29d72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fdf351e-5941-4c9f-ac0b-ed19db7c1bde",
                    "LayerId": "fcb240d9-f829-4268-9fa1-322c7314492d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "fcb240d9-f829-4268-9fa1-322c7314492d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2ff1639a-c32a-4f19-a2e5-c1dc36c286d0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}