{
    "id": "48e38f7c-0885-4287-a936-9eb89aef8277",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_SlimBorder2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 199,
    "bbox_left": 0,
    "bbox_right": 499,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e09003df-300d-4f3f-b910-cd526ce44de5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48e38f7c-0885-4287-a936-9eb89aef8277",
            "compositeImage": {
                "id": "1916d72f-f478-418a-ace8-987f80b0df6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e09003df-300d-4f3f-b910-cd526ce44de5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "049f64f3-55c1-44c0-9a50-95f14d3eb7b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e09003df-300d-4f3f-b910-cd526ce44de5",
                    "LayerId": "42a613ac-9b94-46e4-9d7b-3634dfd00e33"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 200,
    "layers": [
        {
            "id": "42a613ac-9b94-46e4-9d7b-3634dfd00e33",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "48e38f7c-0885-4287-a936-9eb89aef8277",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 500,
    "xorig": 0,
    "yorig": 0
}