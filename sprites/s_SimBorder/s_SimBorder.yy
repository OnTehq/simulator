{
    "id": "f8d0dde5-24c4-401b-9543-2f447b90430a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_SimBorder",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 899,
    "bbox_left": 0,
    "bbox_right": 599,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c93ecfa7-c5d2-4375-8b86-418f67d55f6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8d0dde5-24c4-401b-9543-2f447b90430a",
            "compositeImage": {
                "id": "b161a29f-5fd4-45d8-9341-12a7892bce3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c93ecfa7-c5d2-4375-8b86-418f67d55f6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21d337a6-2a28-4b6b-a277-a23282ca7ccd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c93ecfa7-c5d2-4375-8b86-418f67d55f6b",
                    "LayerId": "6019910c-9c47-4e29-b5d2-3ce19100d6c8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 900,
    "layers": [
        {
            "id": "6019910c-9c47-4e29-b5d2-3ce19100d6c8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f8d0dde5-24c4-401b-9543-2f447b90430a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 600,
    "xorig": 0,
    "yorig": 0
}