{
    "id": "e2fa2c12-cd01-403c-9f13-4f1aa0dbc01e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_Client_leaving",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "53c61451-f377-4da9-a879-361915842211",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2fa2c12-cd01-403c-9f13-4f1aa0dbc01e",
            "compositeImage": {
                "id": "449211fd-cf6a-4d8e-9aba-c284c477e962",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53c61451-f377-4da9-a879-361915842211",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32dddd24-b07c-4625-aacf-397294e2c4ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53c61451-f377-4da9-a879-361915842211",
                    "LayerId": "bf64d638-138f-4fa5-86a0-24506189c450"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "bf64d638-138f-4fa5-86a0-24506189c450",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e2fa2c12-cd01-403c-9f13-4f1aa0dbc01e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}