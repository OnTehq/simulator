{
    "id": "75a47687-aaf0-4fe5-849c-540d147a3f79",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_Server_busy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "265ff4c6-5be3-4e18-8569-f830066b66b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75a47687-aaf0-4fe5-849c-540d147a3f79",
            "compositeImage": {
                "id": "9ad86ccf-3494-4da2-a8ef-f642ebbfca78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "265ff4c6-5be3-4e18-8569-f830066b66b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c826e4aa-0b11-496f-b8ef-c16b3e42e348",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "265ff4c6-5be3-4e18-8569-f830066b66b4",
                    "LayerId": "46bdf4ee-badb-4c03-8791-8484ba306471"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "46bdf4ee-badb-4c03-8791-8484ba306471",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "75a47687-aaf0-4fe5-849c-540d147a3f79",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}