{
    "id": "31c9739c-cb7f-4de3-8914-cb7415dd67ec",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_btn",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 0,
    "bbox_right": 199,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bcd52844-72d9-48b2-acdc-b95c641c50e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31c9739c-cb7f-4de3-8914-cb7415dd67ec",
            "compositeImage": {
                "id": "3f3fa4f0-d88c-42a2-852f-10289229de21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bcd52844-72d9-48b2-acdc-b95c641c50e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d1239da-67ca-4b33-b0e4-84fee8afcbe9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bcd52844-72d9-48b2-acdc-b95c641c50e9",
                    "LayerId": "96ff3a3a-634c-4062-90b5-d07f70d65c39"
                }
            ]
        },
        {
            "id": "685e7514-5978-4306-8519-0d6915734dcd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31c9739c-cb7f-4de3-8914-cb7415dd67ec",
            "compositeImage": {
                "id": "de69d72b-774c-4251-bf5d-4d5ed7583c69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "685e7514-5978-4306-8519-0d6915734dcd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "116db935-1b39-49cc-aa04-97c7dae8a60c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "685e7514-5978-4306-8519-0d6915734dcd",
                    "LayerId": "96ff3a3a-634c-4062-90b5-d07f70d65c39"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "96ff3a3a-634c-4062-90b5-d07f70d65c39",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "31c9739c-cb7f-4de3-8914-cb7415dd67ec",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 100,
    "yorig": 40
}