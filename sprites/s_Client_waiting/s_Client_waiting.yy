{
    "id": "9ef7b878-dcc7-4bfc-bde4-6cdde6417bc8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_Client_waiting",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4451971c-487f-469a-a667-d7042efb210e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ef7b878-dcc7-4bfc-bde4-6cdde6417bc8",
            "compositeImage": {
                "id": "4d24263b-6272-4014-a0b2-4c4482d653fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4451971c-487f-469a-a667-d7042efb210e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d695458f-63b2-4962-805d-1099710b45dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4451971c-487f-469a-a667-d7042efb210e",
                    "LayerId": "b55d3098-7e80-4aee-ad62-b35679f0a3e4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b55d3098-7e80-4aee-ad62-b35679f0a3e4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9ef7b878-dcc7-4bfc-bde4-6cdde6417bc8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}